# [Enterprise Web Software Development] Mothra <!-- omit in toc -->

## Contents <!-- omit in toc -->

- [Installation](#installation)
  - [Ubuntu 20.04 LTS](#ubuntu-2004-lts)
  - [Windows 10](#windows-10)
- [Usage](#usage)
  - [Frontend](#frontend)
  - [Backend](#backend)

## Installation

### Ubuntu 20.04 LTS

```bash
sudo apt-get install gcc g++ make
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get install -y nodejs yarn

# Docker
# See details at
# https://docs.docker.com/engine/install/ubuntu
```

### Windows 10


```bash
# Install nodejs and npm, see details at
# https://nodejs.org/en/download/
npm install --global yarn
```

## Usage

### Frontend

```bash
# From src/frontend
yarn install
yarn start
```

### Backend

```bash
# From src/backend
npm run dev
```
