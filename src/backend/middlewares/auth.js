const jwt = require("jsonwebtoken");
const { User } = require("../models");
const { SECRET_KEY } = process.env;

const authTokenMiddleware = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  try {
    if (!authHeader) return res.sendStatus(401);
    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, SECRET_KEY);
    const username = decoded.username;
    const user = await User.findOne({
      username: username,
      "accessTokens.token": token,
    });

    if (!user) return res.status(401).send("Invalid access token.");
    req.token = token;
    next();
  } catch (err) {
    res.status(401).send(err);
  }
};

const authAdminMiddleware = (req, res, next) => {
  const { role } = jwt.decode(req.token);
  role === "admin" ? next() : res.sendStatus(403);
};

module.exports = { authTokenMiddleware, authAdminMiddleware };
