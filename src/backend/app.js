const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const fs = require("fs");

require("./database");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const topicsRouter = require("./routes/topics");
const contributionsRouter = require("./routes/contributions");
const mailRouter = require("./routes/mail");
const tokenRouter = require("./routes/token");

const UPLOAD_FOLDER = process.env.UPLOAD_FOLDER || "uploads";
if (!fs.existsSync(UPLOAD_FOLDER)) fs.mkdirSync(UPLOAD_FOLDER);

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/v1", indexRouter);
app.use("/api/v1/users", usersRouter);
app.use("/api/v1/topics", topicsRouter);
app.use("/api/v1/contributions", contributionsRouter);
app.use("/api/v1/mail", mailRouter);
app.use("/api/v1/token", tokenRouter);

module.exports = app;
