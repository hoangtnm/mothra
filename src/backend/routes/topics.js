const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { ObjectId } = mongoose.Types;
const { User, Topic, Contribution } = require("../models");
const {
  authAdminMiddleware,
  authTokenMiddleware,
} = require("../middlewares/auth");

const router = express.Router();

const authAdminAndCoordinatorMiddleware = async (req, res, next) => {
  const { username } = jwt.decode(req.token);
  const user = await User.findOne({ username });
  const topic = await Topic.findById(req.params.id);
  if (
    user.role === "admin" ||
    (user.role === "marketing coordinator" && user.faculty === topic.faculty)
  ) {
    next();
  } else res.sendStatus(403);
};

const checkTopicExists = (req, res, next) => {
  const { title } = req.body;
  Topic.findOne({ title }, (err, topic) => {
    if (err) return next(err);
    if (topic) {
      res.status(400).json({ error: "That topic is taken. Try another." });
    } else next();
  });
};

const TOPIC_LOOKUP_STAGE = {
  $lookup: {
    from: Topic.collection.name,
    let: { topicId: "$topicId" },
    pipeline: [
      { $match: { $expr: { $eq: ["$_id", "$$topicId"] } } },
      { $unset: ["contributions", "updatedAt", "__v"] },
    ],
    as: "topic",
  },
};

const AUTHOR_LOOKUP_STAGE = {
  $lookup: {
    from: User.collection.name,
    let: { author: "$author" },
    pipeline: [
      { $match: { $expr: { $eq: ["$_id", "$$author"] } } },
      {
        $project: {
          username: 1,
          firstName: 1,
          lastName: 1,
        },
      },
    ],
    as: "author",
  },
};

const CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE = {
  $lookup: {
    from: Contribution.collection.name,
    let: { contributions: "$contributions" },
    pipeline: [
      { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
      { $unset: ["topicId", "__v"] },
      {
        $addFields: {
          submitedAt: {
            $dateToString: { format: "%Y-%m-%d", date: "$createdAt" },
          },
        },
      },
      AUTHOR_LOOKUP_STAGE,
      { $unwind: "$author" },
    ],
    as: "contributions",
  },
};

const CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE = {
  $lookup: {
    from: Contribution.collection.name,
    let: { contributions: "$contributions" },
    pipeline: [
      { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
      { $unset: ["topicId", "comments", "__v"] },
      {
        $addFields: {
          submitedAt: {
            $dateToString: { format: "%Y-%m-%d", date: "$createdAt" },
          },
        },
      },
      AUTHOR_LOOKUP_STAGE,
      { $unwind: "$author" },
    ],
    as: "contributions",
  },
};

// Create new topic
router.post(
  "/upload",
  authTokenMiddleware,
  async (req, res, next) => {
    try {
      const { username, role } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      const { faculty } = req.body;
      if (
        role === "admin" ||
        (role === "marketing coordinator" && user.faculty === faculty)
      ) {
        next();
      } else res.sendStatus(403);
    } catch (err) {
      next(err);
    }
  },
  checkTopicExists,
  (req, res, next) => {
    Topic.create(req.body, (err, topic) => {
      if (err) return next(err);
      res.status(201).json({ topic });
    });
  }
);

router
  .route("/:id")
  .get(authTokenMiddleware, async (req, res, next) => {
    try {
      const { username, role } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      if (["admin", "marketing manager"].includes(role)) {
        const [topic] = await Topic.aggregate([
          { $match: { _id: ObjectId(req.params.id) } },
          role === "admin"
            ? CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE
            : CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE,
        ]);
        topic ? res.status(200).json({ ...topic }) : res.sendStatus(404);
      } else if (role === "marketing coordinator") {
        const [topic] = await Topic.aggregate([
          { $match: { _id: ObjectId(req.params.id) } },
          CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE,
        ]);
        if (topic.faculty === user.faculty) {
          res.status(200).json({ ...topic });
        } else res.sendStatus(403);
      } else if (role === "student") {
        const [topic] = await Topic.aggregate([
          { $match: { _id: ObjectId(req.params.id) } },
          {
            $lookup: {
              from: Contribution.collection.name,
              let: { contributions: "$contributions" },
              pipeline: [
                { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
                { $unset: ["topicId", "comments", "__v"] },
                AUTHOR_LOOKUP_STAGE,
                { $unwind: "$author" },
                { $match: { "author._id": user._id } },
              ],
              as: "contributions",
            },
          },
        ]);
        if (topic.faculty === user.faculty) {
          const contribution = await Contribution.findOne({
            topicId: ObjectId(req.params.id),
          });
          // res.status(200).json({ topic, contribution });
          res.status(200).json({ ...topic });
        } else res.sendStatus(403);
      } else if (role === "guest") {
        const [topic] = await Topic.aggregate([
          { $match: { _id: ObjectId(req.params.id) } },
          {
            $lookup: {
              from: Contribution.collection.name,
              let: { contributions: "$contributions" },
              pipeline: [
                { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
                { $unset: ["topicId", "comments", "__v"] },
                AUTHOR_LOOKUP_STAGE,
                { $unwind: "$author" },
                { $match: { isSelected: true } },
              ],
              as: "contributions",
            },
          },
        ]);
        if (topic.faculty === user.faculty) {
          res.status(200).json({ ...topic });
        } else res.sendStatus(403);
      }
    } catch (err) {
      next(err);
    }
  })
  .put(
    authTokenMiddleware,
    authAdminAndCoordinatorMiddleware,
    async (req, res, next) => {
      try {
        if ("faculty" in req.body) throw new Error("Faculty is non-editable");
        const topic = await Topic.findOne({ title: req.body.title });
        if (topic) throw new Error("That topic title is taken. Try another.");
        Topic.findByIdAndUpdate(
          req.params.id,
          req.body,
          { new: true, runValidators: true },
          (err, topic) => {
            if (err) return next(err);
            res.status(200).json({ ...topic });
          }
        );
      } catch (err) {
        next(err);
      }
    }
  )
  .delete(
    authTokenMiddleware,
    authAdminAndCoordinatorMiddleware,
    (req, res, next) => {
      Topic.findByIdAndDelete(req.params.id, async (err, topic) => {
        try {
          if (err) return next(err);
          await Contribution.deleteMany({ topicId: topic._id });
          res.sendStatus(200);
        } catch (err) {
          next(err);
        }
      });
    }
  );

router
  .route("/process/:year")
  .get(authTokenMiddleware, async (req, res, next) => {
    try {
      const yy = parseInt(req.params.year);
      const { username } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      let topics;
      if (["admin", "marketing manager"].includes(user.role)) {
        topics = await Topic.aggregate([
          { $match: { year: yy } },
          CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE,
        ]);
      } else if (user.role === "marketing coordinator") {
        topics = await Topic.aggregate([
          { $match: { year: yy, faculty: user.faculty } },
          CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE,
        ]);
      } else if (user.role === "student") {
        topics = await Topic.aggregate([
          { $match: { year: yy, faculty: user.faculty } },
          {
            $lookup: {
              from: Contribution.collection.name,
              let: { contributions: "$contributions" },
              pipeline: [
                { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
                { $unset: ["topicId", "__v"] },
                AUTHOR_LOOKUP_STAGE,
                { $unwind: "$author" },
                { $match: { $expr: { $eq: ["$author._id", user._id] } } },
              ],
              as: "contributions",
            },
          },
        ]);
      } else return res.sendStatus(403);
      res.status(200).json({ topics });
    } catch (err) {
      next(err);
    }
  });

router.get(
  "/byFaculty/:faculty",
  authTokenMiddleware,
  async (req, res, next) => {
    const { username, role } = jwt.decode(req.token);
    const user = await User.findOne({ username });
    if (["admin", "marketing manager"].includes(role)) {
      next();
    } else if (["marketing coordinator", "student"]) {
      user.faculty === req.params.faculty
        ? next()
        : res
            .status(403)
            .json({ error: "You don't have access to this faculty." });
    } else res.sendStatus(403);
  },
  async (req, res, next) => {
    try {
      Topic.aggregate([
        {
          $match: {
            faculty: req.params.faculty,
            year: new Date().getFullYear(),
          },
        },
        ["admin", "marketing manager", "marketing coordinator"]
          ? CONTRIBUTION_WITH_COMMENTS_LOOKUP_STAGE
          : CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE,
      ]).exec((err, topics) => {
        if (err) return next(err);
        res.status(200).json({ topics });
      });
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/reports/:year",
  authTokenMiddleware,
  async (req, res, next) => {
    const { role } = jwt.decode(req.token);
    ["admin", "marketing manager", "marketing coordinator", "guest"].includes(
      role
    )
      ? next()
      : res.sendStatus(403);
  },
  async (req, res, next) => {
    try {
      const yy = parseInt(req.params.year);
      const contributions = await Topic.aggregate([
        { $match: { year: yy } },
        CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE,
        { $unwind: "$contributions" },
      ]);
      const numContributions = contributions.length;
      const statistics = await Topic.aggregate([
        { $match: { year: yy } },
        CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE,
        { $unwind: "$contributions" },
        {
          $group: {
            _id: "$faculty",
            topics: { $addToSet: "$_id" },
            contributors: { $addToSet: "$contributions.author._id" },
            totalContributions: { $sum: 1 },
            totalSelectedContributions: {
              $sum: {
                $cond: {
                  if: { $eq: ["$contributions.isSelected", true] },
                  then: 1,
                  else: 0,
                },
              },
            },
          },
        },
        {
          $project: {
            totalTopics: { $size: "$topics" },
            totalContributors: { $size: "$contributors" },
            totalContributions: 1,
            totalSelectedContributions: 1,
            percentage: {
              $round: [
                {
                  $multiply: [
                    {
                      $divide: ["$totalContributions", numContributions],
                    },
                    100,
                  ],
                },
                2,
              ],
            },
          },
        },
      ]);
      const contributionCountPerDate = await Contribution.aggregate([
        { $match: { comments: [] } },
        TOPIC_LOOKUP_STAGE,
        { $unset: "topicId" },
        { $unwind: "$topic" },
        {
          $match: {
            $expr: {
              $eq: ["$topic.year", yy],
            },
          },
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
            value: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            date: "$_id",
            value: 1,
          },
        },
        {
          $sort: {
            date: 1,
          },
        },
      ]);

      const contributionCountPerMonth = await Topic.aggregate([
        { $match: { year: yy } },
        CONTRIBUTION_WITHOUT_COMMENTS_LOOKUP_STAGE,
        { $unwind: "$contributions" },
        {
          $group: {
            _id: {
              month: { $month: "$contributions.createdAt" },
              faculty: "$faculty",
            },
            count: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            month: "$_id.month",
            faculty: "$_id.faculty",
            value: "$count",
          },
        },
        {
          $sort: {
            month: 1,
          },
        },
      ]);
      const contributionsWoComments = await Contribution.aggregate([
        { $match: { comments: [] } },
        TOPIC_LOOKUP_STAGE,
        { $unset: "topicId" },
        { $unwind: "$topic" },
        {
          $match: {
            $expr: {
              $eq: ["$topic.year", yy],
            },
          },
        },
        {
          $addFields: {
            submitedAt: {
              $dateToString: { format: "%Y-%m-%d", date: "$createdAt" },
            },
          },
        },
        AUTHOR_LOOKUP_STAGE,
        { $unwind: "$author" },
      ]);
      const contributionsWoCommentsAfter14Days = await Contribution.aggregate([
        { $match: { comments: [] } },
        TOPIC_LOOKUP_STAGE,
        { $unset: "topicId" },
        { $unwind: "$topic" },
        {
          $match: {
            $expr: {
              $eq: ["$topic.year", parseInt(req.params.year)],
            },
          },
        },
        {
          $addFields: {
            submitedAt: {
              $dateToString: { format: "%Y-%m-%d", date: "$createdAt" },
            },
          },
        },
        AUTHOR_LOOKUP_STAGE,
        { $unwind: "$author" },
        {
          $match: {
            $expr: {
              $gte: ["$$NOW", { $add: ["$createdAt", 14 * 24 * 60 * 60000] }],
            },
          },
        },
      ]);
      const exceptionReports = {
        contributionsWoComments,
        contributionsWoCommentsAfter14Days,
      };
      res.status(200).json({
        statistics,
        exceptionReports,
        contributionCountPerMonth,
        contributionCountPerDate,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.use((err, req, res, next) => {
  res.status(400).json({ error: err.message });
});

module.exports = router;
