const mongoose = require("mongoose");
const express = require("express");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const fs = require("fs");
const { promisify } = require("util");
const unlinkAsync = promisify(fs.unlink);
const AdmZip = require("adm-zip");
const { ObjectId } = mongoose.Types;
const { User, Contribution, Topic } = require("../models");
const { authTokenMiddleware } = require("../middlewares/auth");

const router = express.Router();

const UPLOAD_FOLDER = process.env.UPLOAD_FOLDER || "uploads";
if (!fs.existsSync(UPLOAD_FOLDER)) fs.mkdirSync(UPLOAD_FOLDER);

const upload = multer({
  storage: multer.diskStorage({
    destination(req, file, cb) {
      cb(null, UPLOAD_FOLDER);
    },
    filename(req, file, cb) {
      cb(null, `${new Date().getTime()}_${file.originalname}`);
    },
  }),
  limits: {
    fileSize: 1024 ** 2 * 10,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpeg|jpg|png|pdf|doc|docx)$/)) {
      return cb(new Error("Unsupprted type file."));
    }
    cb(undefined, true); // continue with upload
  },
});

const AUTHOR_LOOKUP_STAGE = {
  $lookup: {
    from: User.collection.name,
    let: { author: "$author" },
    pipeline: [
      { $match: { $expr: { $eq: ["$_id", "$$author"] } } },
      {
        $project: {
          username: 1,
          firstName: 1,
          lastName: 1,
        },
      },
    ],
    as: "author",
  },
};

const checkContributionExists = async (req, res, next) => {
  try {
    const { title } = req.body;
    const contribution = await Contribution.findOne({ title });
    if (contribution)
      throw new Error("That contribution title is taken. Try another.");
    next();
  } catch (err) {
    next(err);
  }
};

const checkValidStudentAndCoordinator = async (req, res, next) => {
  try {
    const { username } = jwt.decode(req.token);
    const user = await User.findOne({ username });
    const contribution = await Contribution.findById(req.params.id);
    const topic = await Topic.findById(contribution.topicId);
    if (
      !(
        ["admin", "marketing manager"].includes(user.role) ||
        contribution.author.equals(user._id) ||
        (user.role === "marketing coordinator" &&
          user.faculty === topic.faculty)
      )
    ) {
      return res.sendStatus(403);
    }
    if (!contribution)
      return res.status(404).json({ error: "Contribution not found." });
    next();
  } catch (err) {
    next(err);
  }
};

// Create new contribution
router.post(
  "/upload",
  upload.fields([
    { name: "articles", maxCount: 3 },
    { name: "images", maxCount: 10 },
  ]),
  authTokenMiddleware,
  checkContributionExists,
  async (req, res, next) => {
    try {
      const { topicId, title, abstract } = req.body;
      const { username } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      const topic = await Topic.findById(topicId);
      if (!topic) return res.status(404).json({ error: "Topic not found." });
      if (!(user.role === "student" && user.faculty === topic.faculty))
        return res.status(403).json({
          error: "You don't have permission to this topic or faculty.",
        });
      if (new Date() > topic.closureDate)
        return res.status(403).json({
          error: "Closure date passed. Cannot upload a new contribution",
        });

      const { articles, images } = req.files;
      if (images) {
        Contribution.create(
          {
            topicId: topic._id,
            title,
            author: user._id,
            abstract,
            articles: articles.map((f) => f.filename),
            images: images.map((f) => f.filename),
          },
          async (err, contribution) => {
            if (err) return next(err);
            topic.contributions.push(contribution._id);
            await topic.save();
            res.status(201).json({ ...contribution.toJSON() });
          }
        );
      } else {
        Contribution.create(
          {
            topicId: topic._id,
            title,
            author: user._id,
            abstract,
            articles: articles.map((f) => f.filename),
          },
          async (err, contribution) => {
            if (err) return next(err);
            topic.contributions.push(contribution._id);
            await topic.save();
            res.status(201).json({ ...contribution.toJSON() });
          }
        );
      }
    } catch (err) {
      next(err);
    }
  }
);

router
  .route("/:id")
  .get(authTokenMiddleware, async (req, res, next) => {
    try {
      const { username, role } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      const contribution = await Contribution.findById(req.params.id);
      const topic = await Topic.findById(contribution.topicId);
      if (
        ["admin", "marketing manager"].includes(role) ||
        (["marketing coordinator", "guest"] &&
          user.faculty === topic.faculty) ||
        (role === "student" && contribution.author.equals(user._id))
      ) {
        const [contribution] = await Contribution.aggregate([
          { $match: { _id: ObjectId(req.params.id) } },
          AUTHOR_LOOKUP_STAGE,
          { $unwind: "$author" },
          { $unwind: { path: "$comments", preserveNullAndEmptyArrays: true } },
          {
            $lookup: {
              from: User.collection.name,
              localField: "comments.postedBy",
              foreignField: "_id",
              as: "comments.postedBy",
            },
          },
          {
            $unwind: {
              path: "$comments.postedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              topicId: 1,
              articles: 1,
              images: 1,
              isSelected: 1,
              title: 1,
              author: 1,
              abstract: 1,
              createdAt: 1,
              "comments.postedBy._id": 1,
              "comments.comment": 1,
              "comments.postedBy.username": 1,
              "comments.postedBy.firstName": 1,
              "comments.postedBy.lastName": 1,
            },
          },
          {
            $group: {
              _id: "$_id",
              title: { $first: "$title" },
              articles: { $first: "$articles" },
              author: { $first: "$author" },
              abstract: { $first: "$abstract" },
              images: { $first: "$images" },
              comments: { $push: "$comments" },
              isSelected: { $first: "$isSelected" },
              createdAt: { $first: "$createdAt" },
            },
          },
        ]);
        return res.status(200).json({ ...contribution });
      } else res.sendStatus(403);
    } catch (err) {
      next(err);
    }
  })
  .put(
    authTokenMiddleware,
    (req, res, next) => {
      const { role } = jwt.decode(req.token);
      ["admin", "student"].includes(role) ? next() : res.sendStatus(403);
    },
    async (req, res, next) => {
      try {
        const contribution = await Contribution.findById(req.params.id);
        const topic = await Topic.findById(contribution.topicId);
        if (!contribution) return res.sendStatus(404);
        if ("topicId" in req.body || "author" in req.body)
          throw new Error("topicId and author are non-editable");
        if (new Date() > topic.finalClosureDate)
          return res.status(403).json({
            error: "Final closure date passed. Cannot update the contribution",
          });
        Contribution.findByIdAndUpdate(
          req.params.id,
          req.body,
          { new: true, runValidators: true },
          (err, contribution) => {
            if (err) return next(err);
            res.status(200).json({ ...contribution.toJSON() });
          }
        );
      } catch (err) {
        next(err);
      }
    }
  )
  .delete(
    authTokenMiddleware,
    async (req, res, next) => {
      try {
        const { username } = jwt.decode(req.token);
        const user = await User.findOne({ username });
        const contribution = await Contribution.findById(req.params.id);
        user.role === "admin" || contribution.author.equals(user._id)
          ? next()
          : res.sendStatus(403);
      } catch (err) {
        next(err);
      }
    },
    (req, res, next) => {
      Contribution.findByIdAndDelete(
        req.params.id,
        async (err, contribution) => {
          try {
            if (err) return next(err);
            const { topicId } = contribution;
            const topic = await Topic.findById(topicId);
            topic.contributions.pull(contribution._id);
            await topic.save();
            res.sendStatus(200);
          } catch (err) {
            next(err);
          }
        }
      );
    }
  );

router.post(
  "/:id/comments",
  authTokenMiddleware,
  checkValidStudentAndCoordinator,
  async (req, res) => {
    try {
      const { username } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      const contribution = await Contribution.findById(req.params.id);
      contribution.comments.push({
        comment: req.body.comment,
        postedBy: user._id,
      });
      await contribution.save();
      res.status(200).json({ ...contribution.toJSON() });
    } catch (error) {
      console.error(error.message);
      res.status(404);
    }
  }
);

router.delete(
  "/:id/comments/:commentId",
  authTokenMiddleware,
  checkValidStudentAndCoordinator,
  async (req, res) => {
    try {
      const { username } = jwt.decode(req.token);
      const user = await User.findOne({ username });
      const contribution = await Contribution.findById(req.params.id);
      const comment = contribution.comments.find((item) =>
        item._id.equals(ObjectId(req.params.commentId))
      );
      if (!comment)
        return res.status(404).json({ error: "Comment not found." });
      if (!comment.postedBy.equals(user._id)) return res.status(403);

      contribution.comments = contribution.comments.filter((comment) => {
        return !comment._id.equals(ObjectId(req.params.commentId));
      });
      await contribution.save();
      res.status(200).json({ ...contribution.toJSON() });
    } catch (error) {
      console.error(error.message);
      if (error.kind == ObjectId) {
        return res.status(404);
      }
      res.status(500);
    }
  }
);

router.post(
  "/download",
  authTokenMiddleware,
  (req, res, next) => {
    const { role } = jwt.decode(req.token);
    ["admin", "marketing manager"].includes(role)
      ? next()
      : res.sendStatus(403);
  },
  async (req, res) => {
    try {
      const { contributionIds } = req.body;

      const to_zip_contributions = fs.readdirSync(UPLOAD_FOLDER);
      let zip_contributions = new AdmZip();

      for (const contributionId of contributionIds) {
        const contribution = await Contribution.findOne({
          _id: new ObjectId(contributionId),
        });
        if (contribution) {
          to_zip_contributions.forEach((file) => {
            const nameFileZip = file.toString();
            const checkNameZip =
              contribution.images.includes(nameFileZip) ||
              contribution.articles.includes(nameFileZip);
            if (checkNameZip) {
              zip_contributions.addLocalFile(UPLOAD_FOLDER + "/" + file);
            }
          });
        }
      }

      const downloadName = `downloads.zip`;
      // const data = zip_contribution.toBuffer();

      zip_contribution.writeZip(downloadName);
      // res.download(downloadName, { root: "." });
      res.download(downloadName);
      // fs.unlinkSync(downloadName);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  }
);

router.post("/:id/toggleIsSelected", authTokenMiddleware, async (req, res) => {
  try {
    const { username } = jwt.decode(req.token);
    const user = await User.findOne({ username });
    const tmpcontribution = await Contribution.findById(req.params.id);
    const topic = await Topic.findById(tmpcontribution.topicId);

    if (
      user.role === "admin" ||
      !(user.role === "marketing coordinator" && user.faculty === topic.faculty)
    ) {
      return res.sendStatus(403);
    }

    const update = { isSelected: req.body.isSelected };
    const contribution = await Contribution.findByIdAndUpdate(
      req.params.id,
      update,
      {
        new: true,
        runValidators: true,
      }
    );
    return res.status(200).json({ ...contribution.toJSON() });
  } catch (err) {
    return res.status(500).json({ msg: err.message });
  }
});

router.post(
  "/:id/update",
  upload.fields([{ name: "articles", maxCount: 3 }]),
  authTokenMiddleware,
  (req, res, next) => {
    const { role } = jwt.decode(req.token);
    ["admin", "student"].includes(role) ? next() : res.sendStatus(403);
  },
  async (req, res, next) => {
    try {
      const { title, abstract } = req.body;
      const { articles } = req.files;
      const contribution = await Contribution.findById(req.params.id);
      const topic = await Topic.findById(contribution.topicId);
      if (!contribution) return res.sendStatus(404);
      if ("topicId" in req.body || "author" in req.body)
        throw new Error("topicId and author are non-editable");
      if (new Date() > topic.finalClosureDate)
        return res.status(403).json({
          error: "Final closure date passed. Cannot update the contribution",
        });
      if (articles) {
        const item = {
          title,
          abstract,
          articles: articles.map((f) => f.filename),
        };
        Contribution.findOneAndUpdate(
          { _id: req.params.id },
          { $set: item },
          { new: false, upsert: true },
          (err, contribution) => {
            if (err) return next(err);
            unlinkAsync(contribution.articles.map((f) => f.filename));
            return res.status(200).json({ ...contribution.toJSON() });
          }
        );
      } else {
        const item = {
          title,
          abstract,
        };
        Contribution.findOneAndUpdate(
          { _id: req.params.id },
          { $set: item },
          { new: false, upsert: true },
          (err, contribution) => {
            if (err) return next(err);
            return res.status(200).json({ ...contribution.toJSON() });
          }
        );
      }
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/:id/download",
  authTokenMiddleware,
  checkValidStudentAndCoordinator,
  async (req, res) => {
    try {
      const contribution = await Contribution.findById(req.params.id);

      if (!contribution)
        return res.status(404).json({ error: "Contribution not found." });

      const path = UPLOAD_FOLDER;
      if (fs.existsSync(path)) {
        const to_zip_contribution = fs.readdirSync(UPLOAD_FOLDER);
        const zip_contribution = new AdmZip();
        to_zip_contribution.forEach((file) => {
          const nameFileZip = file.toString();
          const checkNameZip =
            contribution.images.includes(nameFileZip) ||
            contribution.articles.includes(nameFileZip);
          if (checkNameZip) {
            zip_contribution.addLocalFile(UPLOAD_FOLDER + "/" + file);
          }
        });

        const downloadName = `${req.params.id}.zip`;
        // const data = zip_contribution.toBuffer();

        zip_contribution.writeZip(downloadName);
        // res.download(downloadName, { root: "." });
        res.download(downloadName);
        // fs.unlinkSync(downloadName);
      } else {
        res.status(404).json({ err: err.message });
      }
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
);

router.get("/publication/:year", async (req, res, next) => {
  try {
    const yy = parseInt(req.params.year);
    const topics = await Topic.aggregate([
      { $match: { year: yy } },
      {
        $lookup: {
          from: Contribution.collection.name,
          let: { contributions: "$contributions" },
          pipeline: [
            { $match: { $expr: { $in: ["$_id", "$$contributions"] } } },
            { $unset: ["topicId", "comments", "__v"] },
            {
              $addFields: {
                submitedAt: {
                  $dateToString: { format: "%Y-%m-%d", date: "$createdAt" },
                },
              },
            },
            AUTHOR_LOOKUP_STAGE,
            { $unwind: "$author" },
            { $match: { isSelected: true } },
          ],
          as: "contributions",
        },
      },
      { $match: { $expr: { $not: { $eq: ["$contributions", []] } } } },
    ]);
    return res.status(200).json({ topics });
  } catch (err) {
    next(err);
  }
});

router.get("/publication/:year/:id/download", async (req, res, next) => {
  try {
    const yy = parseInt(req.params.year);
    const topic = await Topic.findOne({ year: yy });

    const contribution = await Contribution.findOne({
      isSelected: true,
    });

    const tmpContribution = await Contribution.findById(req.params.id);

    if (contribution && topic) {
      const to_zip_contribution = fs.readdirSync(UPLOAD_FOLDER);
      const zip_contribution = new AdmZip();
      to_zip_contribution.forEach((file) => {
        const nameFileZip = file.toString();
        const checkNameZip =
          tmpContribution.images.includes(nameFileZip) ||
          tmpContribution.articles.includes(nameFileZip);
        if (checkNameZip) {
          zip_contribution.addLocalFile(UPLOAD_FOLDER + "/" + file);
        }
      });

      const downloadName = `${req.params.id}.zip`;
      // const data = zip_contribution.toBuffer();

      zip_contribution.writeZip(downloadName);
      // res.download(downloadName, { root: "." });
      res.download(downloadName);
      // fs.unlinkSync(downloadName);
    } else {
      res.status(404);
    }
  } catch (err) {
    next(err);
  }
});

router.use((err, req, res, next) => {
  res.status(400).json({ error: err.message });
});

module.exports = router;
