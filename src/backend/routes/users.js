const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const passwordValidator = require("password-validator");
const { User } = require("../models");
const {
  authTokenMiddleware,
  authAdminMiddleware,
} = require("../middlewares/auth");

const router = express.Router();

const PASSWORD_SCHEMA = new passwordValidator();
PASSWORD_SCHEMA.is()
  .min(8)
  .is()
  .max(100)
  .has()
  .uppercase()
  .has()
  .lowercase()
  .has()
  .digits(2)
  .has()
  .not()
  .spaces()
  .is()
  .not()
  .oneOf(["Passw0rd", "Password123"]);

const clearUserTokens = async (username) => {
  const user = await User.findOne({ username });
  user.accessTokens.splice(0, user.accessTokens.length);
  user.refreshTokens.splice(0, user.refreshTokens.length);
  await user.save();
};

const checkUserExists = async (req, res, next) => {
  try {
    const { faculty, role } = req.body;
    if (role === "marketing manager") {
      const user = await User.findOne({ role });
      if (user) throw new Error("Marketing manager already exists.");
    } else if (role === "marketing coordinator") {
      if (faculty === "n/a") {
        throw new Error(`Faculty of ${role} must be different from n/a`);
      }
      const user = await User.findOne({ faculty, role });
      if (user) throw new Error(`${faculty} faculty had ${role}.`);
    }

    const user = await User.findOne({ username: req.body.username });
    if (user) throw new Error("That username is taken. Try another.");
    next();
  } catch (err) {
    next(err);
  }
};

// root account
router.post("/signup", checkUserExists, (req, res, next) => {
  if (req.body.username === "root") {
    User.create(req.body, (err, user) => {
      if (err) return next(err);
      res.status(201).send({ user });
    });
  } else next("route");
});

router.post(
  "/signup",
  authTokenMiddleware,
  authAdminMiddleware,
  checkUserExists,
  (req, res, next) => {
    const { password } = req.body;
    if (!PASSWORD_SCHEMA.validate(password))
      throw new Error("Invalid password");
    User.create(req.body, (err, user) => {
      if (err) return next(err);
      res.status(201).json({ user });
    });
  }
);

router.get("/all", authTokenMiddleware, async (req, res, next) => {
  const { username } = jwt.decode(req.token);
  const user = await User.findOne({ username });
  let users;
  if (["admin", "marketing manager"].includes(user.role)) {
    users = await User.aggregate([
      { $match: { _id: { $exists: true } } },
      {
        $project: {
          username: 1,
          firstName: 1,
          lastName: 1,
          faculty: 1,
          role: 1,
          email: 1,
          phone: 1,
        },
      },
    ]);
  } else if (user.role === "marketing coordinator") {
    users = await User.aggregate([
      { $match: { _id: { $exists: true }, faculty: user.faculty } },
      {
        $project: {
          username: 1,
          firstName: 1,
          lastName: 1,
          faculty: 1,
          role: 1,
          email: 1,
          phone: 1,
        },
      },
    ]);
  } else return res.sendStatus(403);
  res.status(200).json({ users });
});

router
  .route("/:username")
  .get(authTokenMiddleware, (req, res) => {
    const { username, role } = jwt.decode(req.token);
    if (req.params.username === username || role === "admin") {
      User.findOne({ username: req.params.username }, (err, user) => {
        if (err) return next(err);
        if (user) {
          const { _id, accessTokens, refreshTokens, ...rest } = user.toJSON();
          res.status(200).json({ ...rest });
        } else res.sendStatus(404);
      });
    } else res.sendStatus(403);
  })
  .put(authTokenMiddleware, async (req, res, next) => {
    try {
      const { username, role } = jwt.decode(req.token);
      if (req.params.username === username || role === "admin") {
        // if ("role" in req.body && req.body.role === "marketing manager") {
        //   const user = await User.findOne({ role: "marketing  manager" });
        //   if (user) throw new Error("Marketing manager already exists.");
        // }
        // if ("role" in req.body && req.body.role === "marketing coordinator") {
        //   if (!("faculty" in req.body)) throw new Error("Faculty is required.");
        //   const user = await User.findOne({
        //     faculty: req.body.faculty,
        //     role: req.body.role,
        //   });
        //   if (user) throw new Error("This faculty's role already exists.");
        // }
        if ("role" in req.body || "faculty" in req.body) {
          throw new Error("Faculty and role are non-editable.");
        }

        if ("password" in req.body) {
          if (!PASSWORD_SCHEMA.validate(req.body.password))
            throw new Error("Invalid password");
          await clearUserTokens(req.params.username);
          req.body.password = await bcrypt.hash(req.body.password, 8);
        }
        User.findOneAndUpdate(
          { username: req.params.username },
          req.body,
          { new: true, runValidators: true },
          (err, user) => {
            if (err) return next(err);
            res.status(200).json({ user });
          }
        );
      } else res.sendStatus(403);
    } catch (err) {
      next(err);
    }
  })
  .delete(authTokenMiddleware, authAdminMiddleware, (req, res, next) => {
    User.deleteOne({ username: req.params.username }, (err) => {
      if (err) return next(err);
      res.sendStatus(200);
    });
  });

router.post("/login", async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findByCredentials(username, password);
    const tokens = await user.generateAuthToken();
    res.status(200).json({ ...tokens });
  } catch (err) {
    res.status(401).json({ error: err.message });
  }
});

router.post("/logout", authTokenMiddleware, async (req, res) => {
  try {
    const { username } = jwt.decode(req.token);
    const user = await User.findOne({ username });
    user.accessTokens = user.accessTokens.filter((token) => {
      return token.token != req.token;
    });
    user.refreshTokens = user.refreshTokens.filter((token) => {
      return token.token != req.body.refreshToken;
    });
    await user.save();
    res.sendStatus(200);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.post("/logoutAll", authTokenMiddleware, async (req, res) => {
  try {
    const { username } = jwt.decode(req.token);
    await clearUserTokens(username);
    res.sendStatus(200);
  } catch (err) {
    res.status(500).send({ error: err.message });
  }
});

router.use((err, req, res, next) => {
  res.status(400).json({ error: err.message });
});

module.exports = router;
