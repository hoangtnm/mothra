const mongoose = require("mongoose");
const express = require("express");
const jwt = require("jsonwebtoken");
const { User, Contribution } = require("../models");
const { ObjectId } = mongoose.Types;
const { authTokenMiddleware } = require("../middlewares/auth");
const { MAIL_TEMPLATE } = process.env;
const { MAIL_USERNAME } = process.env;
const { MAIL_PASSWORD } = process.env;

const nodemailer = require("nodemailer");

const router = express.Router();

const option = {
  service: "gmail",
  auth: {
    user: MAIL_USERNAME,
    pass: MAIL_PASSWORD,
  },
};

const transporter = nodemailer.createTransport(option);

router.post(
  "/",
  authTokenMiddleware,
  (req, res, next) => {
    const { contributionId } = req.body;
    Contribution.findById(contributionId, (err, contribution) => {
      if (err) return next(err);
      contribution ? next() : res.sendStatus(403);
    });
  },
  async (req, res) => {
    try {
      const { username } = jwt.decode(req.token);
      const { faculty } = await User.findOne({ username });
      const { email } = await User.findOne({
        faculty,
        role: "marketing coordinator",
      });
      res.status(200).json({ email });
      transporter.verify(function (error, success) {
        if (error) {
          console.log(error);
        } else {
          console.log("Successful connection!");
          let message = {
            from: "MOTHRA SYSTEM <khanhndqgcd17030@gmail.com>",
            to: email,
            subject: "Notification_Email ",
            html: { path: MAIL_TEMPLATE },
          };
          transporter.sendMail(message, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log("Email sent: " + info.response);
            }
          });
        }
      });
    } catch (err) {
      res.status(500).send({ error: err.message });
    }
  }
);

module.exports = router;
