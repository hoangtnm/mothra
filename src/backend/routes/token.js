var express = require("express");
const jwt = require("jsonwebtoken");
const { SECRET_KEY, REFRESH_KEY } = process.env;
const { User } = require("../models");

const router = express.Router();

router.post("/refresh", async (req, res) => {
  try {
    const { refreshToken } = req.body;
    if (!refreshToken)
      return res.status(401).json({ error: "refreshToken is required" });

    const { username } = jwt.decode(refreshToken);
    let user = await User.findOne({ username });
    let { role, refreshTokens } = user;
    refreshTokens = refreshTokens.map((item) => item.token);
    if (!refreshTokens.includes(refreshToken)) {
      return res.status(403).json({ error: "refreshToken is invalid" });
    }

    jwt.verify(refreshToken, REFRESH_KEY, async (err, decoded) => {
      if (err) return res.status(403).json({ error: err.message });
      const accessToken = jwt.sign({ username, role }, SECRET_KEY, {
        expiresIn: "5m",
      });
      user.accessTokens.push({ token: accessToken });
      await user.save();
      res.status(201).json({ accessToken });
    });
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
