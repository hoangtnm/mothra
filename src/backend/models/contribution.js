const mongoose = require("mongoose");
const { Schema } = mongoose;

const contributionSchema = new Schema(
  {
    topicId: { type: Schema.Types.ObjectId, ref: "Topic" },
    title: { type: String, required: true, unique: true, trim: true },
    author: { type: Schema.Types.ObjectId, ref: "User" },
    abstract: { type: String, required: true, trim: true },
    articles: [{ type: String, required: true }],
    images: [{ type: String, required: true }],
    comments: [
      {
        comment: { type: String, trim: true },
        postedBy: { type: Schema.Types.ObjectId, ref: "User" },
        createdAt: { type: Date, default: Date.now },
      },
    ],
    isSelected: { type: Boolean, default: false },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Contribution", contributionSchema);
