const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { Schema } = mongoose;
const { SECRET_KEY, REFRESH_KEY } = process.env;

const userSchema = new Schema({
  username: { type: String, required: true, unique: true, trim: true },
  password: { type: String, required: true, minLength: 8 },
  firstName: String,
  lastName: String,
  dateOfBirth: Date,
  phone: { type: Number, length: 10 },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: (value) => {
      return validator.isEmail(value);
    },
  },
  faculty: {
    type: String,
    required: true,
    lowercase: true,
    enum: ["n/a", "bus", "ehhs", "es", "las"],
  },
  role: {
    type: String,
    required: true,
    enum: [
      "admin",
      "marketing manager",
      "marketing coordinator",
      "student",
      "guest",
    ],
  },
  accessTokens: [{ token: { type: String, required: true } }],
  refreshTokens: [{ token: { type: String, required: true } }],
  isEnabled: { type: Boolean, default: true },
});

// Hash the password before saving
userSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, 8);
  }
  next();
});

userSchema.methods.generateAuthToken = async function () {
  const accessToken = jwt.sign(
    { username: this.username, role: this.role },
    SECRET_KEY,
    { expiresIn: "5m" }
  );
  const refreshToken = jwt.sign(
    { username: this.username, role: this.role },
    REFRESH_KEY
  );
  this.accessTokens = this.accessTokens.concat({ token: accessToken });
  this.refreshTokens = this.refreshTokens.concat({ token: refreshToken });
  await this.save();
  return { accessToken, refreshToken };
};

userSchema.statics.findByCredentials = async function (username, password) {
  const user = await this.findOne({ username });
  if (!user) {
    throw new Error("Invalid login credentials");
  }
  const isPasswordMatched = await bcrypt.compare(password, user.password);
  if (!isPasswordMatched) {
    throw new Error("Invalid login credentials");
  }
  return user;
};

module.exports = mongoose.model("User", userSchema);
