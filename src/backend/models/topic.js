const mongoose = require("mongoose");
const { Schema } = mongoose;

const topicSchema = new Schema(
  {
    title: { type: String, required: true, unique: true, trim: true },
    description: { type: String, default: "N/A", trim: true },
    faculty: {
      type: String,
      required: true,
      lowercase: true,
      enum: ["bus", "ehhs", "es", "las"],
    },
    year: { type: Number, min: 2020 },
    closureDate: { type: Date, required: true },
    finalClosureDate: { type: Date, required: true },
    contributions: [{ type: Schema.Types.ObjectId, ref: "Contribution" }],
    isPublished: Boolean,
  },
  { timestamps: true }
);

topicSchema.pre("save", async function (next) {
  if (new Date(this.closureDate) <= new Date(this.createdAt)) {
    throw new Error("Invalid closure date");
  }
  if (new Date(this.finalClosureDate) <= new Date(this.closureDate)) {
    throw new Error("Invalid final closure date");
  }
  this.year = new Date().getFullYear();
  next();
});

module.exports = mongoose.model("Topic", topicSchema);
