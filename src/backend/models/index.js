module.exports = {
  User: require("./user"),
  Contribution: require("./contribution"),
  Topic: require("./topic"),
};
