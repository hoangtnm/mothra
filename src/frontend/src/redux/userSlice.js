import { createSlice } from "@reduxjs/toolkit";

const getInitialState = () => ({
  username: "",
  accessToken: "",
  refreshToken: "",
  role: "",
  userAvatar: "",
  isAuthenticated: false,
  isFetching: false,
  isSubmiting: false,
  error: "",
});

const userSlice = createSlice({
  name: "user",
  initialState: getInitialState(),
  reducers: {
    setAuthenticated(state, action) {
      state.isAuthenticated = true;
    },
    saveToken(state, action) {
      const { accessToken, refreshToken } = action.payload;
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;
    },
    baseRequest(state, action) {
      state.isFetching = true;
    },
    baseFailure(state, action) {
      state.error = action.payload.error;
    },
    saveInfo(state, action) {
      const { username, role } = action.payload;
      state.username = username;
      state.role = role;
    },
    clearInfo(state, action) {
      state = getInitialState();
    },
  },
});

export const {
  setAuthenticated,
  saveToken,
  saveInfo,
  baseRequest,
  baseFailure,
  clearInfo,
} = userSlice.actions;
export default userSlice.reducer;
