import React, { useState } from "react";
import { Modal, Button, Tooltip } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";
import { LoremIpsum } from "lorem-ipsum";

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4,
  },
  wordsPerSentence: {
    max: 16,
    min: 4,
  },
});

const DocumentHelper = (props) => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <Tooltip title="Help!">
        <Button
          shape="circle"
          type="text"
          onClick={() => setVisible(true)}
          style={{ color: "#A7A7A7", fontSize: 12 }}
        >
          {props.title}
        </Button>
      </Tooltip>
      <Modal
        title="Documentation"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={1000}
      >
        <h3>Documentation</h3>
        <p>{lorem.generateParagraphs(2)}</p>
        <p>{lorem.generateParagraphs(3)}</p>
        <p>{lorem.generateParagraphs(2)}</p>
      </Modal>
    </>
  );
};

export default DocumentHelper;
