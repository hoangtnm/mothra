import React from "react";
import { Space } from "antd";

const IconText = ({ icon, text, ...args }) => (
  <Space>
    {React.createElement(icon, { ...args })}
    {text}
  </Space>
);

export default IconText;
