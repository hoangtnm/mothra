import React, { useState, useLayoutEffect } from "react";
import { Layout, Menu, Avatar, Space, Dropdown, message } from "antd";
import {
  UserOutlined,
  LogoutOutlined,
  TableOutlined,
  AreaChartOutlined,
  ContainerOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import userAPI from "services/userAPI";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { clearInfo } from "../redux/userSlice";

const { Header, Sider, Content, Footer } = Layout;

const accessableRoles = ["admin", "marketing manager", "marketing coordinator"];

const BasicLayout = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const refreshToken = localStorage.getItem("refreshToken");
  //Header: user dropdown menu
  const menu = (
    <Menu>
      <Menu.Item>
        <Link to={`/users/${props.username}`}>
          <UserOutlined />
          Profile
        </Link>
      </Menu.Item>
      <Menu.Item danger>
        <Link
          to="#"
          onClick={async () => {
            try {
              await userAPI.logout(refreshToken);
              history.push("/users/login");
              localStorage.clear();
              dispatch(clearInfo());
            } catch (err) {
              console.log({ err });
              const response = { err };
              if (response) {
                message.error(response.err.response.data.error);
              }
            }
          }}
        >
          <LogoutOutlined /> Log out
        </Link>
      </Menu.Item>
    </Menu>
  );

  const useWindowWidth = () => {
    const [width, setWidth] = useState(0);
    useLayoutEffect(() => {
      function updateWidth() {
        setWidth([window.innerWidth]);
      }
      window.addEventListener("resize", updateWidth);
      updateWidth();
      return () => window.removeEventListener("resize", updateWidth);
    }, []);
    return width;
  };

  const deviceWidth = useWindowWidth() > 670 ? "80" : "0";
  //Hook for collapsed menu
  const [isCollapsed, setCollapsed] = useState(true);
  const [isHovered, setHovered] = useState(false);
  const toggleMenu = () => {
    setCollapsed(!isCollapsed);
  };
  const toggleHover = () => {
    setHovered(!isHovered);
  };
  const headerStyle = isHovered
    ? {
        float: "right",
        marginRight: 20,
        marginTop: 7,
        width: 230,
        justifyContent: "center",
        backgroundColor: "#EFF0F1",
        height: 50,
        borderRadius: 10,
      }
    : {
        float: "right",
        marginRight: 20,
        marginTop: 7,
        width: 230,
        justifyContent: "center",
        backgroundColor: "#FFFFFF",
        height: 50,
        borderRadius: 10,
      };

  const contentStyle =
    useWindowWidth() > 670
      ? { margin: "0px 0px 0 80px", overflow: "initial" }
      : { margin: "0px 5px 16px 0", overflow: "initial" };
  return (
    <Layout>
      {/* Right-side */}
      <Sider
        collapsedWidth={deviceWidth}
        collapsible
        collapsed={isCollapsed}
        onCollapse={toggleMenu}
        style={{
          height: "100vh",
          position: "fixed",
          zIndex: 999,
        }}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            color: "white",
            textAlign: "center",
            fontSize: 25,
            fontWeight: 900,
          }}
        >
          {isCollapsed ? "M" : "Mothra"}
        </div>
        {/* Menu */}
        <Menu theme="dark" mode="inline" selectedKeys="false">
          {accessableRoles.includes(props.role) || props.role === "guest" ? (
            <Menu.Item
              key="1"
              icon={<AreaChartOutlined />}
              onClick={() => history.push("/dashboard")}
            >
              Chart
            </Menu.Item>
          ) : null}
          {accessableRoles.includes(props.role) ? (
            <Menu.Item
              key="2"
              icon={<UserOutlined />}
              onClick={() => history.push("/users/management")}
            >
              User
            </Menu.Item>
          ) : null}
          {accessableRoles.includes(props.role) ? (
            <Menu.Item
              key="3"
              icon={<TableOutlined />}
              onClick={() => history.push("/topics/management")}
            >
              Topics
            </Menu.Item>
          ) : null}
          {accessableRoles.includes(props.role) || props.role === "student" ? (
            <Menu.Item
              key="4"
              icon={<ContainerOutlined />}
              onClick={() => history.push("/contributions/management")}
            >
              Contributions
            </Menu.Item>
          ) : null}
        </Menu>
      </Sider>
      {/* Left-side */}
      <Layout className="site-layout">
        <Header style={{ padding: 0, background: "#fff" }}>
          <Space
            size={"small"}
            style={headerStyle}
            onMouseOver={toggleHover}
            onMouseOut={toggleHover}
          >
            <Avatar size={30} icon={<UserOutlined />} />
            <Dropdown overlay={menu} placement="bottomCenter" arrow>
              <p
                style={{
                  marginBottom: 0,
                  fontFamily: "Helvetica",
                  color: "#002140",
                  fontSize: "1em",
                  fontWeight: 600,
                }}
              >
                {props.username}
              </p>
            </Dropdown>
          </Space>
        </Header>
        {/* Change content here */}
        <Content style={contentStyle}>{props.children}</Content>
        <Footer style={{ textAlign: "center" }}>
          Mothra ©2021 Created by Mothra team
        </Footer>
      </Layout>
    </Layout>
  );
};

export default BasicLayout;
