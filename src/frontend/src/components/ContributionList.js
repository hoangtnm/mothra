// import { useState } from "react";
import { Link } from "react-router-dom";
import {
  MessageOutlined,
  DownloadOutlined,
  CheckSquareOutlined,
  ExclamationCircleFilled,
  ClockCircleOutlined,
} from "@ant-design/icons";
import { Button, Card, List, Tag } from "antd";
import IconText from "components/IconText";
import layoutStyles from "styles/BasicLayout.module.css";
import FileDownload from "js-file-download";
import contributionAPI from "services/contributionAPI";

const ContributionList = ({ title, contributions }) => {
  // const [checkedContributions, setCheckedContributions] = useState([]);
  const onDownload = async (item) => {
    const res = await contributionAPI.downloadContributionById(item._id);
    FileDownload(res, `${item.author}_${item.title}_${item._id}.zip`);
  };
  const onDownloadAll = async () => {
    const downloadPromises = contributions.map((item) => {
      const res = contributionAPI.downloadContributionById(item._id);
      FileDownload(res, `${item.author}_${item.title}_${item._id}.zip`);
    });
    Promise.all(downloadPromises);
  };
  return (
    <Card
      title={title}
      style={{ marginTop: "24px" }}
      extra={
        <Button
          type="primary"
          shape="round"
          icon={<DownloadOutlined />}
          size="middle"
          onClick={onDownloadAll}
        >
          Download (
          {/* {checkedContributions.length > 0
            ? checkedContributions.length
            : contributions.length} */}
          {contributions.length} )
        </Button>
      }
    >
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 5,
        }}
        dataSource={contributions}
        renderItem={(item) => (
          <List.Item
            key={item._id}
            actions={[
              <IconText
                icon={MessageOutlined}
                text={item.comments.length}
                key="list-vertical-message"
              />,
              item.isSelected ? (
                <IconText
                  icon={CheckSquareOutlined}
                  text="selected"
                  key="list-vertical-message"
                  color="success"
                />
              ) : (
                <IconText
                  icon={ClockCircleOutlined}
                  text="waiting"
                  key="list-vertical-message"
                />
              ),
              <div onClick={() => onDownload(item)}>
                <IconText
                  icon={DownloadOutlined}
                  text="download"
                  key="list-vertical-message"
                />
              </div>,
            ]}
          >
            <List.Item.Meta
              title={
                <Link to={`/contributions/${item._id}`}>{item.title}</Link>
              }
              description={
                <div>
                  <Tag color="processing">{item.topic.faculty}</Tag>
                  <Tag color="default">{item.topic.title}</Tag>
                  {item.comments.length === 0 && (
                    <Tag icon={<ExclamationCircleFilled />} color="warning">
                      No comment
                    </Tag>
                  )}
                </div>
              }
            />
            {item.abstract}
            <div className={layoutStyles.cardListItemContentIndexExtra}>
              <span
                stype
              >{`${item.author.firstName} ${item.author.lastName} (${item.author.username})`}</span>
              <em className={layoutStyles.em}>{item.submitedAt}</em>
            </div>
          </List.Item>
        )}
      ></List>
    </Card>
  );
};

export default ContributionList;
