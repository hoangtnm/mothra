import React, { useState, useEffect } from "react"
import styles from "./search.module.css"
import { SearchOutlined } from '@ant-design/icons';
import { Input } from "antd";
const Search = () => {
    const [term, setTerm] = useState("")

    useEffect(() => {

    }, [])

    return (
        <div>

            <div className={styles.search_form}>
                    
                    <div className={styles.search_field}>
                        <Input prefix={<SearchOutlined />}
                            className={styles.input}
                            value={term}
                            onChange={e => setTerm(e.target.value)}
                            placeholder="Search.."
                        />
                    </div>
            </div>
        </div>
    )
}
export default Search