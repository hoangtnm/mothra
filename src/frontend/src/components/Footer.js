import React from "react";
import { Row, Col } from "antd";
import {
  FacebookFilled,
  TwitterOutlined,
  InstagramOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import DocumentHelper from "./DocumentHelper";

import "./footer.css";
function Footer() {
  return (
    <footer id="footer" className="dark footerLanding">
      <div className="footer-wrap">
        <Row>
          <div style={{ color: "#A7A7A7" }} className="footer-left-content">
            <div className="footer-center footer-left container">
              <h2>Mothra</h2>
              <div className="footer-text">
                <p>
                  <span>Mothra</span> is a Lorem Ipsum is simply <br /> dummy
                  text.
                </p>
              </div>
              <div className="social-network">
                <ul>
                  <li>
                    <Link to="/" className="facebook">
                      <FacebookFilled style={{ fontSize: "20px" }} />
                      <span
                        style={{
                          display: "inline-block",
                          verticalAlign: "3px",
                          paddingLeft: "10px",
                        }}
                      >
                        Facebook
                      </span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/" className="twitter">
                      <TwitterOutlined
                        style={{ fontSize: "20px", color: "white" }}
                      />{" "}
                      <span
                        style={{
                          display: "inline-block",
                          verticalAlign: "3px",
                          paddingLeft: "10px",
                        }}
                      >
                        Twitter
                      </span>
                    </Link>
                  </li>
                  <li>
                    <Link to="/" className="instagram">
                      <InstagramOutlined style={{ fontSize: "20px" }} />
                      <span
                        style={{
                          display: "inline-block",
                          verticalAlign: "3px",
                          paddingLeft: "10px",
                        }}
                      >
                        Instagram
                      </span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-right-content">
            <div
              className="footer-center footer-right container"
              style={{ textAlign: "left" }}
            >
              <h2>Help</h2>
              <div>
                <Link to="/" style={{ color: "#A7A7A7" }}>
                  <DocumentHelper title="FAQ" />
                </Link>
              </div>
              <div>
                <Link to="/" style={{ color: "#A7A7A7" }}>
                  <DocumentHelper title="Tutorials" />
                </Link>
              </div>
              <div>
                <Link to="/" style={{ color: "#A7A7A7" }}>
                  <DocumentHelper title="About Us" />
                </Link>
              </div>
              <div>
                <Link to="/" style={{ color: "#A7A7A7" }}>
                  <DocumentHelper title="Privacy Policy" />
                </Link>
              </div>
              <div>
                <Link to="/" style={{ color: "#A7A7A7" }}>
                  <DocumentHelper title="Terms of Service" />
                </Link>
              </div>
            </div>
          </div>
        </Row>
      </div>
      <Row className="bottom-bar" style={{ textAlign: "center" }}>
        <Col lg={18} sm={24} className="copyright">
          <span style={{ marginRight: 12, color: "#A7A7A7" }}>
            Made with <span style={{ color: "red" }}>❤</span> by{" "}
            <span style={{ color: "#fff" }}>Mothra team</span>
          </span>
        </Col>
      </Row>
    </footer>
  );
}
export default Footer;
