// import Search from "./Search";
import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { Row, Col, Menu, Popover } from "antd";
import { MenuOutlined } from "@ant-design/icons";
import MothraLogo from "../assets/MothraLogo.png";
import "./header.css";
import { enquireScreen } from "enquire-js";

const LOGO_URL = MothraLogo;

const Header = () => {
  const [menuVisible, setMenuVisible] = useState(false);
  const [menuMode, setMenuMode] = useState("horizontal");
  const checkVisible = () => setMenuVisible(!menuVisible);
  useEffect(() => {
    enquireScreen((b) => {
      setMenuMode(b ? "inline" : "horizontal");
    });
  }, []);
  const menu = (
    <Menu mode={menuMode} id="nav" key="nav">
      <Menu.Item key="home">
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="publication">
          <Link to="/publication/2021">Publication</Link>
      </Menu.Item>
      <Menu.Item key="docs">
          <Link to="/">Docs</Link>
      </Menu.Item>
    </Menu>
  );
  return (
    <div id="header" className="header">
      {menuMode === "inline" && (
        <Popover
          overlayClassName="popover-menu"
          placement="bottomRight"
          content={menu}
          trigger="click"
          visible={menuVisible}
          arrowPointAtCenter
          onVisibleChange={checkVisible}
        >
          <MenuOutlined className="nav-phone-icon" type="menu" />
        </Popover>
      )}

      <Row>
        <Col xxl={4} xl={5} lg={8} md={8} sm={24} xs={24}>
          <div id="logo" to="/">
            <img src={LOGO_URL} alt="Mothra" />
          </div>
        </Col>

        <Col xxl={20} xl={19} lg={16} md={16} sm={0} xs={0}>
          {/* <Search /> */}
          <div className="headerMeta">
            {menuMode === "horizontal" ? <div id="menu">{menu}</div> : null}
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Header;
