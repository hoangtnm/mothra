import { useHistory } from "react-router-dom";
import { Button, Result } from "antd";
import BasicLayout from "../components/BasicLayout";
import { useSelector } from "react-redux";
import Image403 from "../assets/Image403.png";
import styles from "./error.module.css";

const NoAuthorizedPage = () => {
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div className={styles.image}>
        <img src={Image403} alt="403" className={styles.errorImage} />
      </div>
      <div className={styles.errorbtn}>
        <h2>We are Sorry...</h2>
        <p>You don't have access to this page.</p>
        <Button type="primary" onClick={() => history.push("/")} className={styles.backhomebtn} >
          Back Home
        </Button>
      </div>
    </BasicLayout>
  );
};

export default NoAuthorizedPage;
