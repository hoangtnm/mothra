import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Input,
  Space,
  List,
  Tag,
  Card,
  Breadcrumb,
  Tabs,
  message,
  Menu,
} from "antd";
import { Link } from "react-router-dom";
import { DownloadOutlined } from "@ant-design/icons";
import styles from "pages/Contribution/ContributionManagement.module.css";
import BasicLayout from "components/BasicLayout";
import { useSelector } from "react-redux";
import contributionAPI from "services/contributionAPI";
import facultyConverter from "utils/facultyConversion";
import FileDownload from "js-file-download";
import standardStyles from "styles/StandardStyles.module.css";
import layoutStyles from "styles/BasicLayout.module.css";
import _ from "lodash";

const { TabPane } = Tabs;
const publicationMenu = (
  <Menu>
    {_.range(2020, new Date().getFullYear() + 1).map((year) => (
      <Menu.Item>
        <Link to={`/publication/${year}`}>{year}</Link>
      </Menu.Item>
    ))}
  </Menu>
);

// const itemRender = (route, params, routes, paths) => {
//   const last = routes.indexOf(route) === routes.length - 1;
//   return last ? (
//     <span>{route.breadcrumbName}</span>
//   ) : (
//     <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
//   );
// };

const faculties = [
  { name: "Business School", alias: "bus" },
  { name: "Faculty of Education, Health and Human Science", alias: "ehhs" },
  { name: "Faculty of Engineering and Science", alias: "es" },
  { name: "Faculty of  Liberal and Sciences", alias: "las" },
];

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

const Publication = () => {
  const userInfo = useSelector((state) => state.user);
  const { year } = useParams();
  const [fetchedTopics, setFetchedTopics] = useState([]);
  const [selectedFaculty, setSelectedFaculty] = useState();
  const [
    contributionsOfSelectedFaculty,
    setContributionsOfSelectedFaculty,
  ] = useState([]);

  const [searchValue, setSearchValue] = useState();
  useEffect(() => {
    let mounted = true;
    const fetchData = async () => {
      try {
        const { topics } = await contributionAPI.getPublicationByYear(year);
        if (!topics.length > 0)
          return message.error("This publication is not available.");
        const tmpFaculty = topics[0].faculty;
        const tmpTopics = topics.filter(
          (topic) => topic.faculty === tmpFaculty
        );
        if (mounted) {
          setFetchedTopics(topics);
          setSelectedFaculty(tmpFaculty);
          setContributionsOfSelectedFaculty(
            tmpTopics
              .map((topic) => topic.contributions)
              .filter((contributions) => contributions.length > 0)
              .flat()
          );
        }
      } catch (err) {
        message.error(err.response.data.error);
      }
    };
    fetchData();
    return () => (mounted = false);
  }, [year]);

  const onFacultyTabClick = async (faculty) => {
    try {
      setSelectedFaculty(faculty);
      const tmpTopics = fetchedTopics.filter(
        (topic) => topic.faculty === faculty
      );
      setContributionsOfSelectedFaculty(
        tmpTopics
          .map((topic) => topic.contributions)
          .filter((contributions) => contributions.length > 0)
          .flat()
      );
    } catch (err) {
      console.log(err);
    }
  };
  const FacultyTab = () => (
    <Tabs
      defaultActiveKey={selectedFaculty}
      onTabClick={onFacultyTabClick}
      tabBarStyle={{ marginBottom: 0 }}
    >
      {faculties.map((faculty) => (
        <TabPane
          key={faculty.alias}
          tab={faculty.name}
          style={{ marginBottom: 0 }}
        ></TabPane>
      ))}
    </Tabs>
  );

  const onDownload = async (id) => {
    try {
      const res = await contributionAPI.downloadPublicContributionById(
        year,
        id
      );
      FileDownload(res, `${id}.zip`);
    } catch (err) {
      message.error(err.response.data.error);
    }
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
      >
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/home">Home</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Publication</Breadcrumb.Item>
          <Breadcrumb.Item overlay={publicationMenu}>Year</Breadcrumb.Item>
        </Breadcrumb>
        <Input
          placeholder="Search by title"
          className={styles.searchInput}
          value={searchValue}
          onChange={(e) => {
            const searchInput = e.target.value;
            setSearchValue(searchInput);
            if (searchInput !== "") {
              setContributionsOfSelectedFaculty(
                fetchedTopics
                  .map((contribution) => contribution.contributions)
                  .filter((contributions) => contributions.length > 0)
                  .flat()
                  .filter((contribution) =>
                    contribution.title
                      .toLowerCase()
                      .includes(searchInput.toLowerCase())
                  )
              );
            } else {
              setContributionsOfSelectedFaculty(
                fetchedTopics
                  .filter(
                    (contribution) => contribution.faculty === selectedFaculty
                  )
                  .map((contribution) => contribution.contributions)
                  .filter((contributions) => contributions.length > 0)
                  .flat()
              );
            }
          }}
        />
        <FacultyTab />
      </div>

      <div className={layoutStyles.layout}>
        <div className={layoutStyles.layoutContent}>
          <Card>
            <Space direction="vertical" style={{ width: "100%" }}>
              <List
                itemLayout="vertical"
                size="large"
                pagination={{
                  onChange: (page) => console.log(`Page: ${page}`),
                  pageSize: 5,
                }}
                dataSource={contributionsOfSelectedFaculty}
                renderItem={(item) => (
                  <List.Item
                    key={item._id}
                    actions={[
                      <div onClick={() => onDownload(item._id)}>
                        {" "}
                        <IconText
                          icon={DownloadOutlined}
                          text="download"
                          key="list-vertical-message"
                        />
                      </div>,
                    ]}
                  >
                    <List.Item.Meta
                      title={item.title}
                      description={
                        <div>
                          <Tag color="blue">
                            {facultyConverter[selectedFaculty]}
                          </Tag>
                          <Tag color="default">{item.title}</Tag>
                        </div>
                      }
                    />
                    {item.abstract}
                    <div className={layoutStyles.cardListItemContentIndexExtra}>
                      <span
                        stype
                      >{`${item.author.firstName} ${item.author.lastName} (${item.author.username})`}</span>
                      <em className={layoutStyles.em}>{item.submitedAt}</em>
                    </div>
                  </List.Item>
                )}
              />
            </Space>
          </Card>
        </div>
      </div>
    </BasicLayout>
  );
};

export default Publication;
