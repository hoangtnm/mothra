import { useHistory } from "react-router-dom";
import { Button, Result } from "antd";
import BasicLayout from "../components/BasicLayout";
import { useSelector } from "react-redux";
import Image404 from "../assets/Image404.png";
import styles from "./error.module.css";

const NoFoundPage = () => {
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div className={styles.image}>
        <img src={Image404} alt="404" className={styles.errorImage} />
      </div>
      <div className={styles.errorbtn}>
        <h2>Oops!</h2>
        <p>Sorry, the page you visited does not exist.</p>
        <Button
          type="primary"
          onClick={() => history.push("/")}
          className={styles.backhomebtn}
        >
          Back Home
        </Button>
      </div>
    </BasicLayout>
  );
};

export default NoFoundPage;
