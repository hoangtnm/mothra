import React, { useState, useLayoutEffect, useEffect } from "react";
import styles from "./CreateTopic.module.css";
import BasicLayout from "../../components/BasicLayout";
import { Link } from "react-router-dom";
import {
  Input,
  Button,
  Form,
  Select,
  DatePicker,
  message,
  Breadcrumb,
} from "antd";
import { useSelector } from "react-redux";
import topicAPI from "services/topicAPI";
import { useHistory } from "react-router-dom";
import moment from "moment";
import standardStyles from "styles/StandardStyles.module.css";

const data = {
  title: "",
  description: null,
  faculty: "",
  closureDate: null,
  finalClosureDate: null,
};
const routes = [
  {
    path: `/management`,
    breadcrumbName: "Topics",
  },
  {
    path: "/create-topic",
    breadcrumbName: "Create Topic",
  },
];
const authorizedRole = ["admin", "marketing coordinator"];
const itemRender = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
  );
};
const CreateTopic = () => {
  const history = useHistory();
  const success = () => {
    message.success("Created sucessfully!");
  };
  const useWindowWidth = () => {
    const [width, setWidth] = useState(0);
    useLayoutEffect(() => {
      function updateWidth() {
        setWidth([window.innerWidth]);
      }
      window.addEventListener("resize", updateWidth);
      updateWidth();
      return () => window.removeEventListener("resize", updateWidth);
    }, []);
    return width;
  };
  const labelGrid = { span: 6 };
  const inputGrid = { span: 12 };
  const [form] = Form.useForm();
  const dateFormat = "YYYY-MM-DD";
  const d = new Date();
  const userInfo = useSelector((state) => state.user);
  useEffect(() => {
    const checkAuthorized = () => {
      if (!authorizedRole.includes(userInfo.role)) {
        history.push("/403");
      }
    };
    checkAuthorized();
  });
  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };
  const onFinish = async (values) => {
    data.title = values.title;
    data.description = values.description;
    data.closureDate = values["closureDate"].format(dateFormat);
    data.finalClosureDate = values["finalClosureDate"].format(dateFormat);
    data.faculty = values.faculty;
    try {
      await topicAPI.createTopic(
        data.title,
        data.description,
        data.faculty,
        data.closureDate,
        data.finalClosureDate
      );
      success();
    } catch (err) {
      console.log({ err });
      const response = { err };
      if (response) {
        message.error(response.err.response.data.error);
      }
    }
  };
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
      >
        <Breadcrumb
          itemRender={itemRender}
          routes={routes}
          className={standardStyles.breadcrumb}
        />
        <div className={standardStyles.pageHeaderHeading}>
          <div className={standardStyles.pageHeaderHeadingTitle}>
            Create a New Topic
          </div>
        </div>
        <div className={standardStyles.pageHeaderContent}>
          <div>
            Create a new topic for your particular faculty in this academic
            year.
          </div>
        </div>
      </div>
      <div
        className={styles.site_layout_background}
        style={
          useWindowWidth() > 570
            ? { padding: 24, height: "70vh", margin: "1em 12px" }
            : { padding: 24, margin: "1em 12px" }
        }
      >
        <Form
          labelCol={labelGrid}
          wrapperCol={inputGrid}
          form={form}
          onFinish={onFinish}
          initialValues={{
            year: d.getFullYear(),
          }}
          layout="horizontal"
          size="middle"
        >
          <Form.Item
            name="title"
            label="Title"
            rules={[
              {
                required: true,
                message: "Please input topic name!",
              },
              {
                validator(_, value) {
                  const regex = /^[a-zA-Z0-9-_ ]{4,}[a-zA-Z]+[0-9]*$/;
                  if (regex.test(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Title must not contain special characters.")
                  );
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input.TextArea />
          </Form.Item>
          <Form.Item name="year" label="Academic Year">
            <Input disabled />
          </Form.Item>
          <Form.Item
            name="faculty"
            label="Faculty"
            rules={[
              {
                required: true,
                message: "Please select faculty!",
              },
            ]}
          >
            <Select>
              <Select.Option value="bus">Business School</Select.Option>
              <Select.Option value="ehhs">
                Faculty of Education, Health and Human Science
              </Select.Option>
              <Select.Option value="es">
                Faculty of Engineering and Science
              </Select.Option>
              <Select.Option value="las">
                Faculty of Liberal and Sciences
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="closureDate"
            label="Closure Date"
            rules={[
              {
                type: "date",
                required: true,
                message: "Please select closure date!",
              },
            ]}
          >
            <DatePicker format={dateFormat} disabledDate={disabledDate} />
          </Form.Item>
          <Form.Item
            name="finalClosureDate"
            label="Final Closure Date"
            rules={[
              {
                type: "date",
                required: true,
                message: "Please select final closure date!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("closureDate") < value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Final closure date must after the closure date")
                  );
                },
              }),
            ]}
          >
            <DatePicker format={dateFormat} disabledDate={disabledDate} />
          </Form.Item>
          <Form.Item>
            <Button
              className={styles.topic_Button}
              htmlType="submit"
              type="primary"
            >
              Create
            </Button>
          </Form.Item>
        </Form>
      </div>
    </BasicLayout>
  );
};

export default CreateTopic;
