import React, { useState, useLayoutEffect, useEffect } from "react";
import styles from "./EditTopic.module.css";
import BasicLayout from "../../components/BasicLayout";
import standardStyles from "styles/StandardStyles.module.css";
import { Link } from "react-router-dom";
import {
  Input,
  Button,
  Form,
  Select,
  DatePicker,
  message,
  Breadcrumb,
  Tooltip,
} from "antd";
import { FileAddOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import topicAPI from "services/topicAPI";
import moment from "moment";

const data = {
  title: "",
  description: null,
  faculty: "",
  year: null,
  closureDate: null,
  finalClosureDate: null,
};
const EditTopic = () => {
  const { topicId } = useParams();
  const history = useHistory();
  const useWindowWidth = () => {
    const [width, setWidth] = useState(0);
    useLayoutEffect(() => {
      function updateWidth() {
        setWidth([window.innerWidth]);
      }
      window.addEventListener("resize", updateWidth);
      updateWidth();
      return () => window.removeEventListener("resize", updateWidth);
    }, []);
    return width;
  };
  const success = () => {
    message.success("Edited sucessfully!");
  };
  const formStyle =
    useWindowWidth() > 560
      ? { padding: 24, height: "70vh", margin: "1em 12px" }
      : { padding: 24, margin: "1em 12px" };
  const labelGrid = { span: 6 };
  const inputGrid = { span: 12 };
  const [isGetData, setGetData] = useState(false);
  const [isEditable, setEditable] = useState(true);
  const [form] = Form.useForm();
  const dateFormat = "YYYY-MM-DD";
  const userInfo = useSelector((state) => state.user);
  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };
  useEffect(() => {
    const getData = async () => {
      try {
        const res = await topicAPI.getTopicById(topicId);
        data.title = res.title;
        data.description = res.description;
        data.faculty = res.faculty;
        data.year = res.year;
        data.closureDate = res.closureDate;
        data.finalClosureDate = res.finalClosureDate;

        setGetData(true);
      } catch (err) {
        console.log({ err });
        const response = { err };
        if (response.err.response.status) {
          response.err.response.status === 404 && history.push("/404");
          response.err.response.status === 403 && history.push("/403");
          message.error(response.err.response.data.error);
        }
      }
      userInfo.role === "admin" || userInfo.role === "marketing coordinator"
        ? setEditable(false)
        : setEditable(true);
    };
    getData();
    /* eslint-disable react-hooks/exhaustive-deps */
  }, []);
  const routes = [
    {
      path: `/management`,
      breadcrumbName: "Topics",
    },
    {
      path: "/:topicid",
      breadcrumbName: `${data.title}`,
    },
  ];
  const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
    );
  };
  const onValueChanged = (changedValue, allValues) => {
    data.title = allValues.title;
    data.description = allValues.description;
    if (changedValue.closureDate) {
      data.closureDate = allValues.closureDate.format("YYYY-MM-DD");
    }
    if (changedValue.finalClosureDate) {
      data.finalClosureDate = allValues.finalClosureDate.format("YYYY-MM-DD");
    }
  };
  const onFinish = async () => {
    try {
      await topicAPI.updateTopicById(
        topicId,
        data.description,
        data.closureDate,
        data.finalClosureDate
      );
      success();
      history.push("/topics/management");
    } catch (err) {
      console.log({ err });
      const response = { err };
      if (response) {
        message.error(response.err.response.data.error);
      }
    }
  };
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      {isGetData ? (
        <div
          className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
        >
          {/* <Breadcrumb
            itemRender={itemRender}
            routes={routes}
            className={standardStyles.breadcrumb}
          /> */}
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/">Home</Link>
            </Breadcrumb.Item>
            {["admin", "marketing manager"].includes(userInfo.role) ? (
              <Breadcrumb.Item>
                <Link to="/topics/management">Topics</Link>
              </Breadcrumb.Item>
            ) : (
              <Breadcrumb.Item>
                <Link to="/contributions/management">Topics</Link>
              </Breadcrumb.Item>
            )}
            <Breadcrumb.Item>{data.title}</Breadcrumb.Item>
          </Breadcrumb>
          <div className={standardStyles.pageHeaderHeading}>
            <div className={standardStyles.pageHeaderHeadingTitle}>
              {data.title}
            </div>
          </div>
          <div className={standardStyles.pageHeaderContent}>
            <div>
              Edit an existed topic of a particular faculty in this academic
              year.
            </div>
          </div>
        </div>
      ) : null}
      {isGetData && (
        <div className={styles.site_layout_background} style={formStyle}>
          <Form
            labelCol={labelGrid}
            wrapperCol={inputGrid}
            form={form}
            onFinish={onFinish}
            onValuesChange={onValueChanged}
            layout="horizontal"
            size="middle"
            initialValues={{
              title: data.title,
              description: data.description,
              faculty: data.faculty,
              year: data.year,
              closureDate: moment(data.closureDate),
              finalClosureDate: moment(data.finalClosureDate),
            }}
          >
            <Form.Item
              name="title"
              label="Title"
              rules={[
                {
                  required: true,
                  message: "Please input topic name!",
                },
                {
                  validator(_, value) {
                    const regex = /^[a-zA-Z0-9-_ ]{4,}[a-zA-Z]+[0-9]*$/;
                    if (regex.test(value)) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      new Error("Title must not contain special characters.")
                    );
                  },
                },
              ]}
            >
              <Input disabled />
            </Form.Item>
            <Form.Item name="description" label="Description">
              <Input.TextArea disabled={isEditable} />
            </Form.Item>
            <Form.Item
              name="faculty"
              label="Faculty"
              rules={[
                {
                  required: true,
                  message: "Please select faculty!",
                },
              ]}
            >
              <Select disabled>
                <Select.Option value="bus">Business School</Select.Option>
                <Select.Option value="ehhs">
                  Faculty of Education, Health and Human Science
                </Select.Option>
                <Select.Option value="es">
                  Faculty of Engineering and Science
                </Select.Option>
                <Select.Option value="las">
                  Faculty of Liberal and Sciences
                </Select.Option>
              </Select>
            </Form.Item>
            <Form.Item name="year" label="Year">
              <Input disabled />
            </Form.Item>
            <Form.Item
              name="closureDate"
              label="Closure Date"
              rules={[
                {
                  type: "date",
                  required: true,
                  message: "Please select closure date!",
                },
              ]}
            >
              <DatePicker
                format={dateFormat}
                disabled={isEditable}
                disabledDate={disabledDate}
              />
            </Form.Item>
            <Form.Item
              name="finalClosureDate"
              label="Final Closure Date"
              rules={[
                {
                  type: "date",
                  required: true,
                  message: "Please select final closure date!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("closureDate") < value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      new Error(
                        "Final closure date must after the closure date"
                      )
                    );
                  },
                }),
              ]}
            >
              <DatePicker
                format={dateFormat}
                disabled={isEditable}
                disabledDate={disabledDate}
              />
            </Form.Item>
            {userInfo.role !== "student" && (
              <Form.Item>
                <Button
                  className={styles.topic_Button}
                  htmlType="submit"
                  type="primary"
                  disabled={isEditable}
                >
                  Update
                </Button>
              </Form.Item>
            )}
          </Form>
          {userInfo.role === "student" && (
            <Tooltip title="New contribution">
              <Button
                type="primary"
                shape="circle"
                icon={<FileAddOutlined style={{ fontSize: "30px" }} />}
                style={{
                  zIndex: 1,
                  position: "fixed",
                  right: 30,
                  bottom: 40,
                  width: 60,
                  height: 60,
                  backgroundColor: "#002140",
                  border: "none",
                }}
                onClick={() => history.push(`${topicId}/newContribution`)}
              />
            </Tooltip>
          )}
        </div>
      )}
    </BasicLayout>
  );
};

export default EditTopic;
