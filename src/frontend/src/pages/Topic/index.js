import React from "react";
import { Switch, Route } from "react-router-dom";
import TopicManagement from "./TopicManagement";
import CreateTopic from "./CreateTopic";
import EditTopic from "./EditTopic";
import ContributionUpload from "../Contribution/ContributionUpload";

const Topic = ({ match }) => {
  return (
    <Switch>
      <Route path={`${match.path}/management`} component={TopicManagement} />
      <Route path={`${match.path}/create-topic`} component={CreateTopic} />
      <Route
        exact
        path={`${match.path}/:topicId/newContribution`}
        component={ContributionUpload}
      />
      <Route exact path={`${match.path}/:topicId`} component={EditTopic} />
    </Switch>
  );
};

export default Topic;
