import React, { useState, useEffect } from "react";
import {
  Input,
  Space,
  Button,
  Tabs,
  Table,
  Popconfirm,
  Tooltip,
  Breadcrumb,
  message,
} from "antd";
import { Link } from "react-router-dom";
import { DeleteOutlined, FormOutlined, PlusOutlined } from "@ant-design/icons";
import styles from "./TopicManagement.module.css";
import BasicLayout from "../../components/BasicLayout";
import { useSelector } from "react-redux";
import topicAPI from "services/topicAPI";
import { useHistory } from "react-router-dom";
import userAPI from "services/userAPI";
import FacultyConversion from "utils/facultyConversion";
import standardStyles from "styles/StandardStyles.module.css";

const { TabPane } = Tabs;
const routes = [
  {
    path: `/`,
    breadcrumbName: "Home",
  },
  {
    path: "/topics/management",
    breadcrumbName: "Topics",
  },
];
const itemRender = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
  );
};
const faculties = [
  { name: "Business School", alias: "bus" },
  { name: "Faculty of Education, Health and Human Science", alias: "ehhs" },
  { name: "Faculty of Engineering and Science", alias: "es" },
  { name: "Faculty of  Liberal and Sciences", alias: "las" },
];
const Dashboard = () => {
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  const [topicData, setTopicData] = useState([]);
  const [dataSource, setDataSource] = useState([]);
  const [isGetData, setGetData] = useState(false);
  const [tabActiveKey, setTabActiveKey] = useState(null);
  const [value, setValue] = useState("");
  useEffect(() => {
    const getData = async () => {
      if (userInfo.role === "student") {
        history.push("/403");
      }
      if (userInfo.role === "admin" || userInfo.role === "marketing manager") {
        try {
          const res = await topicAPI.getTopicsByFaculty("bus");
          res.topics.forEach((topic) => {
            topic.facultyName = FacultyConversion["bus"];
          });
          setTopicData(res.topics);
          setDataSource(res.topics);
        } catch (err) {
          console.log({ err });
          const response = { err };
          if (response.err.response) {
            message.error(response.err.response.data.error);
          }
        }
        setGetData(true);
      }
      if (userInfo.role === "marketing coordinator") {
        try {
          const user = await userAPI.getUser(userInfo.username);
          if (user) {
            const res = await topicAPI.getTopicsByFaculty(user.faculty);
            res.topics.forEach((topic) => {
              topic.facultyName = FacultyConversion[user.faculty];
            });
            setTopicData(res.topics);
            setDataSource(res.topics);
          }
        } catch (err) {
          console.log({ err });
          const response = { err };
          if (response.err.response) {
            message.error(response.err.response.data.error);
          }
        }
        setGetData(true);
      }
    };

    getData();
    /* eslint-disable react-hooks/exhaustive-deps */
  }, []);
  const FacultyTab = () => (
    <Tabs
      defaultActiveKey="bus"
      tabBarStyle={{ marginBottom: 0 }}
      activeKey={tabActiveKey}
      onChange={onTabChange}
    >
      {faculties.map((faculty) => (
        <TabPane
          key={faculty.alias}
          tab={faculty.name}
          style={{ marginBottom: 0 }}
        ></TabPane>
      ))}
    </Tabs>
  );
  const onTabChange = async (key) => {
    setTabActiveKey(key);
    try {
      const res = await topicAPI.getTopicsByFaculty(key);
      res.topics.forEach((topic) => {
        topic.facultyName = FacultyConversion[topic.faculty];
      });
      setTopicData(res.topics);
      setDataSource(res.topics);
    } catch (err) {
      console.log({ err });
      const response = { err };
      if (response) {
        message.error(response.err.response.data.error);
      }
    }
    setGetData(true);
  };

  //delete user data function
  const deleteTopic = async (topicData) => {
    try {
      await topicAPI.deleteTopic(topicData._id);
      message.success("Deleted successfully!");
    } catch (err) {
      console.log({ err });
      const response = { err };
      message.error(response.err.response.data.error);
    }
    const deletedData = dataSource.filter((topic) => {
      return topic._id !== topicData._id;
    });
    setDataSource(deletedData);
  };
  const editTopic = (id) => {
    history.push(`/topics/${id}`);
  };
  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      sorter: (a, b) => a.title.localeCompare(b.title),
      width: 500,
    },
    {
      title: "Academic Year",
      dataIndex: "year",
      defaultSortOrder: "descend",
      sorter: (a, b) => a.year - b.year,
      responsive: ["md"],
      width: 150,
      align: "center",
    },
    {
      title: "Faculty",
      dataIndex: "facultyName",
      responsive: ["lg"],
      width: 500,
    },
    {
      title: "",
      key: "action",
      render: (data) => (
        <Space size="middle">
          <Button onClick={() => editTopic(data._id)}>
            <FormOutlined />
          </Button>
          {userInfo.role === "admin" ||
          userInfo.role === "marketing coordinator" ? (
            <Popconfirm
              title="Sure to delete?"
              type="primary"
              onConfirm={() => deleteTopic(data)}
            >
              <Button type="primary" danger>
                <DeleteOutlined />
              </Button>
            </Popconfirm>
          ) : null}
        </Space>
      ),
    },
  ];
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
        style={{
          paddingBottom: userInfo.role === "marketing coordinator" ? 14 : 0,
        }}
      >
        <Breadcrumb
          itemRender={itemRender}
          routes={routes}
          className={standardStyles.breadcrumb}
        />
        <div className={standardStyles.pageHeaderHeading}>
          <div className={standardStyles.pageHeaderHeadingTitle}>Topics</div>
        </div>
        <div className={standardStyles.pageHeaderContent}>
          <div>
            Shown available topics, create, edit or delete topic by using
            shortcuts.
          </div>
        </div>
        <Input
          placeholder="Search by topic title"
          className={styles.searchInput}
          value={value}
          onChange={(e) => {
            const currValue = e.target.value;
            setValue(currValue);
            const filteredData = topicData.filter((entry) =>
              entry.title.includes(currValue)
            );
            setDataSource(filteredData);
          }}
        />
        {userInfo.role === "admin" || userInfo.role === "marketing manager" ? (
          <FacultyTab />
        ) : null}
      </div>

      <div
        className={styles.site_layout_background}
        style={{ padding: "0px 24px 24px 24px", margin: "1em 12px" }}
      >
        {isGetData ? (
          <Table
            columns={columns}
            dataSource={dataSource}
            rowKey={(obj) => obj._id}
          />
        ) : null}
      </div>
      {userInfo.role === "admin" ||
      userInfo.role === "marketing coordinator" ? (
        <Tooltip title="add new topic">
          <Button
            type="primary"
            shape="circle"
            icon={<PlusOutlined style={{ fontSize: "30px" }} />}
            style={{
              zIndex: 1,
              position: "fixed",
              right: 30,
              bottom: 40,
              width: 60,
              height: 60,
              backgroundColor: "#002140",
              border: "none",
            }}
            onClick={() => history.push(`create-topic`)}
          />
        </Tooltip>
      ) : null}
    </BasicLayout>
  );
};

export default Dashboard;
