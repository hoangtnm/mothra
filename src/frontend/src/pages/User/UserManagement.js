import React, { useState, useEffect } from "react";
import {
  Input,
  Space,
  Button,
  Tabs,
  Table,
  Popconfirm,
  Tooltip,
  Breadcrumb,
  message,
} from "antd";
import {
  DeleteOutlined,
  FormOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import styles from "./UserManagement.module.css";
import BasicLayout from "../../components/BasicLayout";
import { useSelector } from "react-redux";
import userAPI from "services/userAPI";
import { useHistory } from "react-router-dom";
import FacultyConversion from "utils/facultyConversion";
import standardStyles from "styles/StandardStyles.module.css";

const { TabPane } = Tabs;

const routes = [
  {
    path: `/`,
    breadcrumbName: "Home",
  },
  {
    path: "/users/management",
    breadcrumbName: "Users",
  },
];

const itemRender = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
  );
};

const faculties = [
  { name: "Business School", alias: "bus" },
  { name: "Faculty of Education, Health and Human Science", alias: "ehhs" },
  { name: "Faculty of Engineering and Science", alias: "es" },
  { name: "Faculty of  Liberal and Sciences", alias: "las" },
];
const UserManagement = () => {
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  const [userData, setUserData] = useState([]);
  const [dataSource, setDataSource] = useState([]);
  const [isGetData, setGetData] = useState(false);
  const [tabActiveKey, setTabActiveKey] = useState(null);
  const [value, setValue] = useState("");
  useEffect(() => {
    const getData = async () => {
      try {
        if (userInfo.role === "student") {
          history.push("/403");
        }
        const res = await userAPI.getAll();
        res.users.forEach((user) => {
          user.fullName = user.firstName + " " + user.lastName;
          user.facultyName = FacultyConversion[user.faculty];
        });
        if (
          userInfo.role === "admin" ||
          userInfo.role === "marketing manager"
        ) {
          setUserData(res.users);
          setDataSource(res.users);
        }
        if (userInfo.role === "marketing coordinator") {
          setUserData(res.users);
          setDataSource(res.users);
        }
      } catch (err) {
        console.log({ err });
        const response = { err };
        if (response) {
          message.error(response.err.response.data.error);
        }
      }
      setGetData(true);
    };
    getData();
    /* eslint-disable react-hooks/exhaustive-deps */
  }, []);
  const FacultyTab = () => (
    <Tabs
      defaultActiveKey="bus"
      tabBarStyle={{ marginBottom: 0 }}
      activeKey={tabActiveKey}
      onChange={onTabChange}
    >
      {faculties.map((faculty) => (
        <TabPane
          key={faculty.alias}
          tab={faculty.name}
          style={{ margin: 0 }}
        ></TabPane>
      ))}
    </Tabs>
  );
  const onTabChange = (key) => {
    setTabActiveKey(key);
    const filteredData = userData.filter((user) => {
      return (user = user.faculty === key);
    });
    setDataSource(filteredData);
  };

  //delete user data function
  const deleteUser = async (userData) => {
    try {
      await userAPI.deleteUser(userData.username);
      message.success("Deleted successfully!");
    } catch (err) {
      console.log({ err });
      const response = { err };
      if (response) {
        message.error(response.err.response.data.error);
      }
    }
    const deletedData = dataSource.filter((user) => {
      return user._id !== userData._id;
    });
    setDataSource(deletedData);
  };
  const editUser = (username) => {
    history.push(`/users/${username}`);
  };
  const columns =
    userInfo.role === "admin"
      ? [
          {
            title: "Username",
            dataIndex: "username",
            sorter: (a, b) => a.username.localeCompare(b.username),
          },
          {
            title: "Name",
            dataIndex: "fullName",
            defaultSortOrder: "descend",
            sorter: (a, b) => a.fullName.length - b.fullName.length,
            responsive: ["md"],
          },
          {
            title: "Role",
            dataIndex: "role",
            responsive: ["md"],
            filters: [
              {
                text: "Marketing Manager",
                value: "marketing manager",
              },
              {
                text: "Marketing Coordinator",
                value: "marketing coordinator",
              },
              {
                text: "Admin",
                value: "admin",
              },
              {
                text: "Student",
                value: "student",
              },
            ],
            filterMultiple: false,
            // specify the condition of filtering result
            // here is that finding the name started with `value`
            onFilter: (value, record) => record.role.indexOf(value) === 0,
            sorter: (a, b) => a.role.length - b.role.length,
            sortDirections: ["descend", "ascend"],
          },
          {
            title: "Faculty",
            dataIndex: "facultyName",
            responsive: ["lg"],
            sorter: (a, b) => a.faculty.length - b.faculty.length,
            sortDirections: ["descend", "ascend"],
          },
          {
            title: "",
            key: "action",
            render: (data) => (
              <Space size="middle">
                <Button onClick={() => editUser(data.username)}>
                  <FormOutlined />
                </Button>
                <Popconfirm
                  title="Sure to delete?"
                  type="primary"
                  onConfirm={() => deleteUser(data)}
                >
                  <Button type="primary" danger>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Space>
            ),
          },
        ]
      : [
          {
            title: "Username",
            dataIndex: "username",
            sorter: (a, b) => a.username.localeCompare(b.username),
          },
          {
            title: "Name",
            dataIndex: "fullName",
            defaultSortOrder: "descend",
            sorter: (a, b) => a.fullName.length - b.fullName.length,
            responsive: ["md"],
          },
          {
            title: "Role",
            dataIndex: "role",
            responsive: ["md"],
            filters: [
              {
                text: "Marketing Manager",
                value: "marketing manager",
              },
              {
                text: "Marketing Coordinator",
                value: "marketing coordinator",
              },
              {
                text: "Admin",
                value: "admin",
              },
              {
                text: "Student",
                value: "student",
              },
            ],
            filterMultiple: false,
            // specify the condition of filtering result
            // here is that finding the name started with `value`
            onFilter: (value, record) => record.role.indexOf(value) === 0,
            sorter: (a, b) => a.role.length - b.role.length,
            sortDirections: ["descend", "ascend"],
          },
          {
            title: "Faculty",
            dataIndex: "facultyName",
            sorter: (a, b) => a.faculty.length - b.faculty.length,
            sortDirections: ["descend", "ascend"],
          },
        ];
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
        style={{
          paddingBottom: userInfo.role === "marketing coordinator" ? 14 : 0,
        }}
      >
        <Breadcrumb
          itemRender={itemRender}
          routes={routes}
          className={standardStyles.breadcrumb}
        />
        <div className={standardStyles.pageHeaderHeading}>
          <div className={standardStyles.pageHeaderHeadingTitle}>Users</div>
        </div>
        <div className={standardStyles.pageHeaderContent}>
          <div>
            Shown available user, create, edit or delete topic by using
            shortcuts.
          </div>
        </div>
        <Input
          placeholder="Search by username"
          className={styles.searchInput}
          value={value}
          onChange={(e) => {
            const currValue = e.target.value;
            setValue(currValue);
            const filteredData = userData.filter((entry) =>
              entry.username.includes(currValue)
            );
            setDataSource(filteredData);
          }}
        />
        {userInfo.role === "admin" || userInfo.role === "marketing manager" ? (
          <FacultyTab />
        ) : null}
      </div>

      <div
        className={styles.site_layout_background}
        style={{ padding: "0px 24px 24px 24px", margin: "1em 12px" }}
      >
        {isGetData ? (
          <Table
            columns={columns}
            dataSource={dataSource}
            rowKey={(obj) => obj._id}
          />
        ) : null}
      </div>
      {userInfo.role === "admin" ? (
        <Tooltip title="add user">
          <Button
            type="primary"
            shape="circle"
            icon={<UserAddOutlined style={{ fontSize: "30px" }} />}
            style={{
              zIndex: 1,
              position: "fixed",
              right: 30,
              bottom: 40,
              width: 60,
              height: 60,
              backgroundColor: "#002140",
              border: "none",
            }}
            onClick={() => history.push(`signup`)}
          />
        </Tooltip>
      ) : null}
    </BasicLayout>
  );
};

export default UserManagement;
