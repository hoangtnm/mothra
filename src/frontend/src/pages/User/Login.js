import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Form, Input, Button, Modal, Layout, message } from "antd";
import userAPI from "services/userAPI";
import { UserOutlined, LockOutlined, GitlabOutlined } from "@ant-design/icons";
import globalStyles from "antd/dist/antd.css";
import styles from "./Login.module.css";
import cx from "classnames";
import { setAuthenticated, saveToken, saveInfo } from "../../redux/userSlice";
import jwt from "jsonwebtoken";
import MothraLogo from "../../assets/MothraLogo.png";

const { Content, Footer } = Layout;

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isLoginFail, setLoginFail] = useState(false);
  const handleSubmit = async (data) => {
    try {
      const res = await userAPI.login(data.username, data.password);
      const { accessToken, refreshToken } = res;
      const decoded = jwt.decode(accessToken);
      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("refreshToken", refreshToken);
      dispatch(saveToken(res));
      dispatch(saveInfo(decoded));
      dispatch(setAuthenticated());
      if (decoded.role === "student") {
        history.push("/contributions/management");
      }
      else if (decoded.role === "guest") {
        history.push("/dashboard");
      } else {
        history.push("/topics/management");
      }
    } catch (err) {
      message.error(err.response.data.error);
      setLoginFail(true);
    }
  };
  return (
    <Layout>
      <Content className={styles.login}>
        <Form
          onFinish={handleSubmit}
          name="normal_login"
          className={styles.login_form}
          initialValues={{
            remember: true,
          }}
        >
          <img
            src={MothraLogo}
            alt="Mothra logo"
            className={styles.login_Logo}
          />
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Link
              to="#"
              className={cx(
                styles.login_replace_right,
                globalStyles["login-form-forgot"]
              )}
              onClick={() => {
                Modal.info({
                  title: "Get password",
                  content:
                    "Please contact admin via email mothra.admin@gmail.com to reset password",
                });
              }}
            >
              Forgot password?
            </Link>
          </Form.Item>
          {isLoginFail ? (
            <Form.Item style={{ margin: 0 }}>
              <p className={styles.errorText}>Wrong username or password</p>
            </Form.Item>
          ) : null}
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className={cx(
                styles.login_replace_button,
                globalStyles["login-form-button"]
              )}
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        Mothra ©2021 Created by Mothra team
        <a
          target="_blank "
          rel="noreferrer"
          href="https://gitlab.com/hoangtnm/mothra"
          style={{ marginLeft: 10 }}
        >
          <GitlabOutlined />
          MothraGitlab
        </a>
      </Footer>
    </Layout>
  );
};

export default Login;
