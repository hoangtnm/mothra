import React, { useEffect, useState } from "react";
import styles from "./ProfileDetail.module.css";
import BasicLayout from "../../components/BasicLayout";
import moment from "moment";
import {
  Input,
  Button,
  Form,
  Select,
  DatePicker,
  message,
  Breadcrumb,
} from "antd";
import { Link } from "react-router-dom";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import userAPI from "services/userAPI";
import passwordValidator from "password-validator";
import { useHistory } from "react-router-dom";
import standardStyles from "styles/StandardStyles.module.css";

const ProfileDetail = () => {
  const history = useHistory();
  const { username } = useParams();
  const userInfo = useSelector((state) => state.user);
  const [data, setData] = useState({});
  const [form] = Form.useForm();
  const [isGetData, setGetData] = useState(false);
  const [isEditable, setEditable] = useState(true);
  const [disabledUpdate, setDisabledUpdate] = useState(true);
  const [isPasswordChange, setPasswordChange] = useState(false);
  const dateFormat = "DD-MM-YYYY";
  const success = () => {
    message.success("Updated sucessfully!");
  };
  const routes = [
    {
      path: `/management`,
      breadcrumbName: "Users",
    },
    {
      path: "/:username",
      breadcrumbName: `${username}`,
    },
  ];

  const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
    );
  };
  const labelGrid = { span: 6 };
  const inputGrid = { span: 12 };
  useEffect(() => {
    const getData = async () => {
      try {
        const res = await userAPI.getUser(username);
        setData(res);
        setGetData(true);
        form.setFieldsValue({
          username: res.username,
          firstName: res.firstName,
          lastName: res.lastName,
          date: moment(res.dateOfBirth),
          faculty: res.faculty,
          role: res.role,
          phone: String(res.phone),
          mail: res.email,
        });
      } catch (err) {
        console.log({ err });
        const response = { err };
        if (response.err.response.status) {
          response.err.response.status === 404 && history.push("/404");
          response.err.response.status === 403 && history.push("/403");
          message.error(response.err.response.data.error);
        }
      }
      userInfo.role === "admin" ? setEditable(false) : setEditable(true);
    };
    getData();
    /* eslint-disable react-hooks/exhaustive-deps */
  }, [username]);
  const onValueChanged = (changedValue, allValues) => {
    data.firstName = allValues.firstName;
    data.lastName = allValues.lastName;
    if (changedValue.date) {
      data.dateOfBirth = allValues.date.format("YYYY-MM-DD");
    }
    if (changedValue.password) setPasswordChange(true);
    if (changedValue.confirm) {
      data.password = allValues.confirm;
      setPasswordChange(true);
    }
    data.phone = parseInt(allValues.phone);
    data.mail = allValues.mail;
    data.role = allValues.role;
    data.faculty = allValues.faculty;
    setDisabledUpdate(false);
  };
  const onUpdate = async (values) => {
    if (isPasswordChange) {
      try {
        await userAPI.updateUserPassword(
          data.username,
          data.password,
          data.firstName,
          data.lastName,
          data.dateOfBirth,
          data.phone,
          data.mail
        );
        success();
        history.push("/users/management");
      } catch (err) {
        console.log({ err });
        const response = { err };
        if (response) {
          message.error(response.err.response.data.error);
        }
      }
    } else {
      try {
        await userAPI.updateUser(
          data.username,
          data.firstName,
          data.lastName,
          data.dateOfBirth,
          data.phone,
          data.mail
        );
        success();
        history.push("/users/management");
      } catch (err) {
        console.log({ err });
        const response = { err };
        if (response) {
          message.error(response.err.response.data.error);
        }
      }
    }
  };
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      {isGetData ? (
        <div>
          <div
            className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
          >
            <Breadcrumb
              itemRender={itemRender}
              routes={routes}
              className={standardStyles.breadcrumb}
            />
            <div className={standardStyles.pageHeaderHeading}>
              <div className={standardStyles.pageHeaderHeadingTitle}>
                {data.firstName} {data.lastName}
              </div>
            </div>
            <div className={standardStyles.pageHeaderContent}>
              <div>Edit information of an existed user.</div>
            </div>
            <p className={styles.title}></p>
          </div>
          <div
            className={styles.site_layout_background}
            style={{ padding: "24px 24px 24px 24px", margin: "1em 12px" }}
          >
            <Form
              labelCol={labelGrid}
              wrapperCol={inputGrid}
              form={form}
              onFinish={onUpdate}
              onValuesChange={onValueChanged}
              layout="horizontal"
              size="middle"
            >
              <Form.Item name="username" label="Username">
                <Input disabled />
              </Form.Item>
              <Form.Item
                name="firstName"
                label="First Name"
                rules={[
                  {
                    required: true,
                    message: "Please input first name!",
                  },
                  {
                    validator(_, value) {
                      const regex = /^(?!\s)([a-z ,.'-]+)$/i;
                      if (regex.test(value)) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error(
                          "First name must not contain special characters and number."
                        )
                      );
                    },
                  },
                ]}
              >
                <Input disabled={isEditable} />
              </Form.Item>
              <Form.Item
                name="lastName"
                label="Last Name"
                rules={[
                  {
                    required: true,
                    message: "Please input last name!",
                  },
                  {
                    validator(_, value) {
                      const regex = /^(?!\s)([a-z ,.'-]+)$/i;
                      if (regex.test(value)) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error(
                          "Last name must not contain special characters and number."
                        )
                      );
                    },
                  },
                ]}
              >
                <Input disabled={isEditable} />
              </Form.Item>
              <Form.Item name="date" label="Date of birth">
                <DatePicker format={dateFormat} disabled={isEditable} />
              </Form.Item>
              <Form.Item
                name="phone"
                label="Phone"
                rules={[
                  {
                    required: true,
                    message: "Please input phone number!",
                  },

                  {
                    len: 10,
                    message: "Phone must have 10 digits!",
                  },
                  {
                    validator(_, value) {
                      if (!isNaN(value)) {
                        return Promise.resolve();
                      }

                      return Promise.reject(new Error("Input must be number"));
                    },
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="mail"
                label="Email"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                  {
                    required: true,
                    message: "Please input E-mail!",
                  },
                ]}
              >
                <Input disabled={isEditable} />
              </Form.Item>
              <Form.Item
                name="faculty"
                label="Faculty"
                rules={[
                  {
                    required: true,
                    message: "Please select faculty!",
                  },
                ]}
              >
                <Select disabled>
                  <Select.Option value="bus">Business School</Select.Option>
                  <Select.Option value="ehhs">
                    Faculty of Education, Health and Human Science
                  </Select.Option>
                  <Select.Option value="es">
                    Faculty of Engineering and Science
                  </Select.Option>
                  <Select.Option value="las">
                    Faculty of Liberal and Sciences
                  </Select.Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="role"
                label="Role"
                rules={[
                  {
                    required: true,
                    message: "Please select role!",
                  },
                ]}
              >
                <Select disabled>
                  <Select.Option value="admin">Admin</Select.Option>
                  <Select.Option value="marketing manager">
                    Marketing Manager
                  </Select.Option>
                  <Select.Option value="marketing coordinator">
                    Marketing Coordinator
                  </Select.Option>
                  <Select.Option value="student">Student</Select.Option>
                  <Select.Option value="guest">Guest</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item
                name="password"
                label="New Password"
                rules={[
                  {
                    min: 8,
                    message: "Password must be at least 8 characters",
                  },
                  {
                    validator(_, value) {
                      const validatePassword = new passwordValidator()
                        .has()
                        .not()
                        .spaces();
                      if (
                        isPasswordChange === false ||
                        validatePassword.validate(value)
                      ) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error("Input must not contain spaces")
                      );
                    },
                  },
                  {
                    validator(_, value) {
                      const validatePassword = new passwordValidator()
                        .is()
                        .max(100);
                      if (
                        isPasswordChange === false ||
                        validatePassword.validate(value)
                      ) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error("Password maximum length is 100 characters")
                      );
                    },
                  },
                  {
                    validator(_, value) {
                      const validatePassword = new passwordValidator()
                        .is()
                        .not()
                        .oneOf(["Passw0rd", "Password123"]);
                      if (
                        isPasswordChange === false ||
                        validatePassword.validate(value)
                      ) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(
                          "Password must not be Passw0rd or Password123"
                        )
                      );
                    },
                  },
                  {
                    validator(_, value) {
                      const validatePassword = new passwordValidator()
                        .has()
                        .digits(2);
                      if (
                        isPasswordChange === false ||
                        validatePassword.validate(value)
                      ) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error("Password must contain at least 2 digits")
                      );
                    },
                  },
                  {
                    validator(_, value) {
                      const validatePassword = new passwordValidator()
                        .has()
                        .uppercase()
                        .has()
                        .lowercase();
                      if (
                        isPasswordChange === false ||
                        validatePassword.validate(value)
                      ) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error(
                          "Password must contain atleast one uppercase and one lowercase letter"
                        )
                      );
                    },
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  type="password"
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                  disabled={isEditable}
                />
              </Form.Item>
              <Form.Item
                name="confirm"
                label="Confirm new Password"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }

                      return Promise.reject(
                        new Error(
                          "The two passwords that you entered do not match!"
                        )
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  type="password"
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                  disabled={isEditable}
                  required={isPasswordChange}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  className={styles.update_Button}
                  htmlType="submit"
                  disabled={disabledUpdate}
                >
                  Update
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      ) : null}
    </BasicLayout>
  );
};

export default ProfileDetail;
