import React, { useEffect } from "react";
import styles from "./Signup.module.css";
import BasicLayout from "../../components/BasicLayout";
import {
  Input,
  Button,
  Form,
  Select,
  DatePicker,
  message,
  Breadcrumb,
} from "antd";
import { Link } from "react-router-dom";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { useSelector } from "react-redux";
import userAPI from "services/userAPI";
import { useHistory } from "react-router-dom";
import passwordValidator from "password-validator";
import standardStyles from "styles/StandardStyles.module.css";

const routes = [
  {
    path: `/management`,
    breadcrumbName: "Users",
  },
  {
    path: "/signup",
    breadcrumbName: "Signup",
  },
];
const itemRender = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
  );
};
const data = {
  username: "",
  password: "",
  firstName: "",
  lastName: "",
  dateOfBirth: null,
  phone: null,
  mail: "",
  role: "",
  faculty: "",
};
const Signup = () => {
  const history = useHistory();
  const [form] = Form.useForm();
  const dateFormat = "DD-MM-YYYY";
  const labelGrid = { span: 6 };
  const inputGrid = { span: 12 };
  const userInfo = useSelector((state) => state.user);
  useEffect(() => {
    const checkAuthorized = () => {
      if (userInfo.role !== "admin") {
        history.push("/403");
      }
    };
    checkAuthorized();
  });
  const success = () => {
    message.success("Signup sucessfully!");
  };
  const onFinish = async (values) => {
    data.username = values.username;
    data.password = values.confirm;
    data.firstName = values.firstName;
    data.lastName = values.lastName;
    data.dateOfBirth = values["date"].format("YYYY-MM-DD");
    data.phone = parseInt(values.phone);
    data.mail = values.mail;
    data.role = values.role;
    data.faculty = values.faculty;
    try {
      await userAPI.addUser(
        data.username,
        data.password,
        data.firstName,
        data.lastName,
        data.dateOfBirth,
        data.phone,
        data.mail,
        data.faculty,
        data.role
      );
      success();
    } catch (err) {
      console.log({ err });
      const response = { err };
      if (response) {
        message.error(response.err.response.data.error);
      }
    }
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
      >
        <Breadcrumb
          itemRender={itemRender}
          routes={routes}
          className={standardStyles.breadcrumb}
        />
        <div className={standardStyles.pageHeaderHeading}>
          <div className={standardStyles.pageHeaderHeadingTitle}>
            Create a New Account
          </div>
        </div>
        <div className={standardStyles.pageHeaderContent}>
          <div>Fill form to create a new account.</div>
        </div>
      </div>
      <div
        className={styles.site_layout_background}
        style={{ padding: "24px", margin: "1em 12px" }}
      >
        <Form
          labelCol={labelGrid}
          wrapperCol={inputGrid}
          form={form}
          onFinish={onFinish}
          layout="horizontal"
          size="middle"
        >
          <Form.Item
            name="username"
            label="Username"
            rules={[
              {
                required: true,
                message: "Please input username!",
              },
              {
                validator(_, value) {
                  const regex = /^(?=[a-z_\d]*[a-z])[a-z_\d]{6,}$/;
                  if (regex.test(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Username must not contain special characters.")
                  );
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="firstName"
            label="First Name"
            rules={[
              {
                required: true,
                message: "Please input first name!",
              },
              {
                validator(_, value) {
                  const regex = /^(?!\s)([a-z ,.'-]+)$/i;
                  if (regex.test(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error(
                      "First name must not contain special characters and number."
                    )
                  );
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="lastName"
            label="Last Name"
            rules={[
              {
                required: true,
                message: "Please input last name!",
              },
              {
                validator(_, value) {
                  const regex = /^(?!\s)([a-z ,.'-]+)$/i;
                  if (regex.test(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error(
                      "Last name must not contain special characters and number."
                    )
                  );
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="date"
            label="Date of birth"
            rules={[
              {
                type: "date",
                required: true,
                message: "Please select date of birth!",
              },
            ]}
          >
            <DatePicker format={dateFormat} />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Phone"
            rules={[
              {
                required: true,
                message: "Please input phone number!",
              },

              {
                len: 10,
                message: "Phone must contain 10 number characters",
              },
              {
                validator(_, value) {
                  if (!isNaN(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error("Input must be number"));
                },
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="mail"
            label="Email"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="faculty"
            label="Faculty"
            rules={[
              {
                required: true,
                message: "Please select faculty!",
              },
            ]}
          >
            <Select>
              <Select.Option value="bus">Business School</Select.Option>
              <Select.Option value="ehhs">
                Faculty of Education, Health and Human Science
              </Select.Option>
              <Select.Option value="es">
                Faculty of Engineering and Science
              </Select.Option>
              <Select.Option value="las">
                Faculty of Liberal and Sciences
              </Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="role"
            label="Role"
            rules={[
              {
                required: true,
                message: "Please select role!",
              },
            ]}
          >
            <Select>
              <Select.Option value="admin">Admin</Select.Option>
              <Select.Option value="marketing manager">
                Marketing manager
              </Select.Option>
              <Select.Option value="marketing coordinator">
                Marketing coordinator
              </Select.Option>
              <Select.Option value="student">Student</Select.Option>
              <Select.Option value="guest">Guest</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please input your Password!",
              },
              {
                min: 8,
                message: "Password must be at least 8 characters",
              },
              {
                validator(_, value) {
                  const validatePassword = new passwordValidator()
                    .has()
                    .not()
                    .spaces();
                  if (validatePassword.validate(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Input must not contain spaces")
                  );
                },
              },
              {
                validator(_, value) {
                  const validatePassword = new passwordValidator()
                    .is()
                    .max(100);
                  if (validatePassword.validate(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Password maximum length is 100 characters")
                  );
                },
              },
              {
                validator(_, value) {
                  const validatePassword = new passwordValidator()
                    .is()
                    .not()
                    .oneOf(["Passw0rd", "Password123"]);
                  if (validatePassword.validate(value)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error("Password must not be Passw0rd or Password123")
                  );
                },
              },
              {
                validator(_, value) {
                  const validatePassword = new passwordValidator()
                    .has()
                    .digits(2);
                  if (validatePassword.validate(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Password must contain at least 2 digits")
                  );
                },
              },
              {
                validator(_, value) {
                  const validatePassword = new passwordValidator()
                    .has()
                    .uppercase()
                    .has()
                    .lowercase();
                  if (validatePassword.validate(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error(
                      "Password must contain atleast one uppercase and one lowercase letter"
                    )
                  );
                },
              },
            ]}
            hasFeedback
          >
            <Input.Password
              type="password"
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error(
                      "The two passwords that you entered do not match!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password
              type="password"
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item>
            <Button className={styles.signup_Button} htmlType="submit">
              Create
            </Button>
          </Form.Item>
        </Form>
      </div>
    </BasicLayout>
  );
};

export default Signup;
