import React from "react";
import { Switch, Route } from "react-router-dom";
import Signup from "./Signup";
import UserManagement from "./UserManagement";
import ProfileDetail from "./ProfileDetail";

const User = ({ match }) => {
  return (
    <Switch>
      <Route path={`${match.path}/signup`} component={Signup} />
      <Route path={`${match.path}/management`} component={UserManagement} />
      <Route path={`${match.path}/:username`} component={ProfileDetail} />
    </Switch>
  );
};

export default User;
