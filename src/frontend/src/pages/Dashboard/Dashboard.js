import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import BasicLayout from "components/BasicLayout";
import topicAPI from "services/topicAPI";
import facultyConverter from "utils/facultyConversion";
import monthConverter from "utils/monthConverter";
import { DualAxes, Line, Pie } from "@ant-design/charts";
import { Breadcrumb, Row, Col, Skeleton, Menu } from "antd";
import standardStyles from "styles/StandardStyles.module.css";
import layoutStyles from "styles/BasicLayout.module.css";
import _ from "lodash";
import ContributionList from "components/ContributionList";

const Dashboard = () => {
  const [isFetching, setIsFetching] = useState(true);
  const [statisticsData, setStatisticsData] = useState();
  const [exceptionReportsData, setExceptionReportsData] = useState();
  const [
    contributionCountPerMonthData,
    setContributionCountPerMonthData,
  ] = useState();
  const [
    contributionCountPerDateData,
    setContributionCountPerDateData,
  ] = useState();
  const [year, setYear] = useState(new Date().getFullYear());
  const userInfo = useSelector((state) => state.user);
  useEffect(() => {
    let isMounted = true;
    const fetchReports = async () => {
      try {
        setIsFetching(true);
        const data = await topicAPI.getReports(year);
        const {
          statistics,
          exceptionReports,
          contributionCountPerMonth,
          contributionCountPerDate,
        } = data;
        statistics.forEach((item) => {
          item.facultyAlias = _.upperFirst(item._id) + ".";
          item.faculty = facultyConverter[item._id];
          item.contributions = item.totalContributions;
          item.selected = item.totalSelectedContributions;
          console.log(item.facultyAlias);
        });
        exceptionReports.contributionsWoComments.forEach(
          (item) => (item.topic.faculty = facultyConverter[item.topic.faculty])
        );
        exceptionReports.contributionsWoCommentsAfter14Days.forEach(
          (item) => (item.topic.faculty = facultyConverter[item.topic.faculty])
        );
        contributionCountPerMonth.forEach((item) => {
          item.month = monthConverter[item.month];
          item.faculty = facultyConverter[item.faculty];
        });
        if (isMounted) {
          setStatisticsData(statistics);
          setExceptionReportsData(exceptionReports);
          setContributionCountPerMonthData(contributionCountPerMonth);
          setContributionCountPerDateData(contributionCountPerDate);
          setIsFetching(false);
        }
      } catch (err) {
        console.log({ err });
      }
    };
    fetchReports();
    return () => (isMounted = false);
  }, [year]);

  const reportMenu = (
    <Menu>
      {_.range(2020, new Date().getFullYear() + 1).map((year) => (
        <Menu.Item>
          <p onClick={() => setYear(year)}>{year}</p>
        </Menu.Item>
      ))}
    </Menu>
  );

  const contributionPerFacultyPieConfig = {
    appendPadding: 10,
    data: statisticsData,
    angleField: "totalContributions",
    colorField: "faculty",
    radius: 0.9,
    label: {
      type: "inner",
      offset: "-30%",
      content: function content(_ref) {
        var percent = _ref.percent;
        return "".concat((percent * 100).toFixed(0), "%");
      },
      style: {
        fontSize: 14,
        textAlign: "center",
      },
    },
    interactions: [{ type: "element-active" }],
  };
  const contributorsPerFacultyPieConfig = {
    appendPadding: 10,
    data: statisticsData,
    angleField: "totalContributors",
    colorField: "faculty",
    radius: 0.9,
    label: {
      type: "inner",
      offset: "-30%",
      content: function content(_ref) {
        var percent = _ref.percent;
        return "".concat((percent * 100).toFixed(0), "%");
      },
      style: {
        fontSize: 14,
        textAlign: "center",
      },
    },
    interactions: [{ type: "element-active" }],
  };
  const contributionCountPerMonthConfig = {
    data: contributionCountPerMonthData,
    xField: "month",
    yField: "value",
    seriesField: "faculty",
    yAxis: {
      label: {
        formatter: function formatter(v) {
          return v;
        },
      },
    },
    legend: { position: "top" },
    smooth: true,
    animation: {
      appear: {
        animation: "path-in",
        duration: 5000,
      },
    },
  };
  const contributionCountPerDateConfig = {
    data: contributionCountPerDateData,
    padding: "auto",
    xField: "date",
    yField: "value",
    xAxis: { tickCount: 5 },
    slider: {
      start: 0.1,
      end: 0.5,
    },
  };
  const contributionsCountDualAxesConfig = {
    data: [statisticsData, statisticsData],
    xField: "facultyAlias",
    yField: ["contributions", "selected"],
    yAxis: {
      value: {
        min: 0,
        label: {
          formatter: function formatter(val) {
            return "".concat(val, "个");
          },
        },
      },
      count: false,
    },
    geometryOptions: [
      {
        geometry: "column",
        color: "#5B8FF9",
        columnWidthRatio: 0.4,
        label: { position: "middle" },
      },
      {
        geometry: "line",
        smooth: true,
        color: "#5AD8A6",
      },
    ],
    interactions: [{ type: "element-highlight" }, { type: "active-region" }],
  };
  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      {!isFetching ? (
        <div>
          <div
            className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
          >
            {/* <Breadcrumb
              itemRender={itemRender}
              routes={routes}
              className={standardStyles.breadcrumb}
            /> */}
            <Breadcrumb>
              <Breadcrumb.Item>
                <Link to="/home">Home</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              <Breadcrumb.Item overlay={reportMenu}>Year</Breadcrumb.Item>
            </Breadcrumb>

            <div className={standardStyles.pageHeaderHeading}>
              <div className={standardStyles.pageHeaderHeadingTitle}>
                Dashboard
              </div>
            </div>
            <div className={standardStyles.pageHeaderContent}>
              <div>
                Charts and diagrams are used to visualize statistics on
                contributions, contributors within each Faculty for each
                academic year.
              </div>
            </div>
          </div>
          <div className={layoutStyles.layout}>
            <div className={layoutStyles.layoutContent}>
              <div style={{ margin: "0px -12px" }}>
                <Row>
                  <Col
                    xs={24}
                    xl={8}
                    sm={12}
                    md={12}
                    lg={12}
                    style={{
                      paddingLeft: "12px",
                      paddingRight: "12px",
                      marginBottom: "24px",
                    }}
                  >
                    <div className={layoutStyles.card}>
                      <div className={layoutStyles.cardBody}>
                        <div className={layoutStyles.chartCard}>
                          <div className={layoutStyles.chartCardIndexChartTop}>
                            <div className={layoutStyles.chartCardIndexMeta}>
                              <span>The Proportion Of Contributions</span>
                            </div>
                            <div className={layoutStyles.chartCardIndexTotal}>
                              <span>
                                {statisticsData.reduce(
                                  (a, b) => a + (b["totalContributions"] || 0),
                                  0
                                )}{" "}
                                Contributions
                              </span>
                            </div>
                          </div>
                          <Pie {...contributionPerFacultyPieConfig} />
                          <div className={layoutStyles.chartCardIndexFooter}>
                            <div>
                              <span className={layoutStyles.chartFieldLabel}>
                                Daily Contributions
                              </span>
                              <span className={layoutStyles.chartFieldNumber}>
                                {Math.round(
                                  contributionCountPerDateData.reduce(
                                    (a, b) => a + (b["value"] || 0),
                                    0
                                  ) / contributionCountPerDateData.length
                                )}{" "}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={24}
                    xl={8}
                    sm={12}
                    md={12}
                    lg={12}
                    style={{
                      paddingLeft: "12px",
                      paddingRight: "12px",
                      marginBottom: "24px",
                    }}
                  >
                    <div className={layoutStyles.card}>
                      <div className={layoutStyles.cardBody}>
                        <div className={layoutStyles.chartCard}>
                          <div className={layoutStyles.chartCardIndexChartTop}>
                            <div className={layoutStyles.chartCardIndexMeta}>
                              <span>The Proportion Of Contributors</span>
                            </div>
                            <div className={layoutStyles.chartCardIndexTotal}>
                              {statisticsData.reduce(
                                (a, b) => a + (b["totalContributors"] || 0),
                                0
                              )}{" "}
                              Contributiors
                            </div>
                          </div>
                          <Pie {...contributorsPerFacultyPieConfig} />
                          <div className={layoutStyles.chartCardIndexFooter}>
                            <div>
                              <span className={layoutStyles.chartFieldLabel}>
                                Total Topics
                              </span>
                              <span className={layoutStyles.chartFieldNumber}>
                                {statisticsData.reduce(
                                  (a, b) => a + (b["totalTopics"] || 0),
                                  0
                                )}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col
                    xs={24}
                    xl={8}
                    sm={12}
                    md={12}
                    lg={12}
                    style={{
                      paddingLeft: "12px",
                      paddingRight: "12px",
                      marginBottom: "24px",
                    }}
                  >
                    <div className={layoutStyles.card}>
                      <div className={layoutStyles.cardBody}>
                        <div className={layoutStyles.chartCard}>
                          <div className={layoutStyles.chartCardIndexChartTop}>
                            <div className={layoutStyles.chartCardIndexMeta}>
                              <span>The Proportion Of Contributors</span>
                            </div>
                            <div className={layoutStyles.chartCardIndexTotal}>
                              {statisticsData.reduce(
                                (a, b) => a + (b["totalContributors"] || 0),
                                0
                              )}{" "}
                              Contributiors
                            </div>
                          </div>
                          <DualAxes {...contributionsCountDualAxesConfig} />
                          <div className={layoutStyles.chartCardIndexFooter}>
                            <div>
                              <span className={layoutStyles.chartFieldLabel}>
                                Total Topics
                              </span>
                              <span className={layoutStyles.chartFieldNumber}>
                                {statisticsData.reduce(
                                  (a, b) => a + (b["totalTopics"] || 0),
                                  0
                                )}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col
                    xs={24}
                    xl={24}
                    sm={24}
                    md={24}
                    lg={24}
                    style={{
                      paddingLeft: "12px",
                      paddingRight: "12px",
                      marginBottom: "24px",
                    }}
                  >
                    <div className={layoutStyles.card}>
                      <div className={layoutStyles.cardBody}>
                        <div className={layoutStyles.chartCard}>
                          <div className={layoutStyles.chartCardIndexChartTop}>
                            <div className={layoutStyles.chartCardIndexMeta}>
                              <span>Real-time Contribution Statistics</span>
                            </div>
                          </div>
                          <Line {...contributionCountPerDateConfig} />
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col
                    xs={24}
                    xl={24}
                    sm={24}
                    md={24}
                    lg={24}
                    style={{
                      paddingLeft: "12px",
                      paddingRight: "12px",
                      marginBottom: "24px",
                    }}
                  >
                    <div className={layoutStyles.card}>
                      <div className={layoutStyles.cardBody}>
                        <div className={layoutStyles.chartCard}>
                          <div className={layoutStyles.chartCardIndexChartTop}>
                            <div className={layoutStyles.chartCardIndexMeta}>
                              <span>Contributions within Faculties Trend</span>
                            </div>
                            <div className={layoutStyles.chartCardIndexTotal}>
                              <span>{100} Contributions</span>
                            </div>
                          </div>
                          <Line {...contributionCountPerMonthConfig} />
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
              {!(userInfo.role === "guest") && (
                <div>
                  <ContributionList
                    title="Contributions without Comments"
                    contributions={exceptionReportsData.contributionsWoComments}
                  />
                  <ContributionList
                    title="Contributions without Comments after 14 Days"
                    contributions={
                      exceptionReportsData.contributionsWoCommentsAfter14Days
                    }
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <Skeleton />
      )}
    </BasicLayout>
  );
};

export default Dashboard;
