import React from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import Illustration1 from "../assets/Illustration1.png";
import Header from "../components/Header";
import Footer from "../components/Footer";
import pic1 from "../assets/pic1.png";
import { CheckCircleFilled } from "@ant-design/icons";
import bannerbottom from "../assets/bannerbottom.png";
import "./Home.css";
import jwt from "jsonwebtoken";

const Home = () => {
  const history = useHistory();
  const handleGetStartedBtn = () => {
    try {
      const token = localStorage.getItem("accessToken");
      if (token) {
        const { role } = jwt.decode(token);
        if (["admin", "marketing manager"].includes(role)) {
          history.push("/dashboard");
        } else if (["marketing coordinator", "student"].includes(role)) {
          history.push("/contributions/management");
        } else history.push("/");
      } else history.push("/users/login");
    } catch (err) {
      history.push("/users/login");
    }
  };

  return (
    <React.Fragment>
      <Header />

      <header className="header_1">
        <div className="header_content">
          <h1>
            Think outside <br />
            the box with <br />
            Mothra
          </h1>
          <p style={{ fontSize: "20px" }}>
            <span style={{ color: "#F53838" }}>Get connected</span> with your{" "}
            <br />
            school{" "}
          </p>
          <div onClick={handleGetStartedBtn} style={{padding: 0}}>
            <Link to="">Get Started</Link>
          </div>
        </div>
        <div className="header_img">
          <img src={Illustration1} alt="Illustration" />
        </div>
      </header>

      <main className="main">
        <section className="services">
          <div className="service-container">
            <div className="services-card service-one">
              <img src={pic1} alt="pic1" />
            </div>
            <div className="service-description">
              <h3>
                We Provide Many <br />
                Features You Can Use
              </h3>
              <div className="service-description-top">
                <p>
                  You can explore the features that we provide with fun and have
                  their own functions each feature.
                </p>
              </div>
              <div>
                <ul>
                  <li>
                    <p>
                      <CheckCircleFilled
                        style={{ color: "#31A652", paddingRight: "10px" }}
                      />
                      <span style={{ paddingRight: "30px" }}>
                        Upload articles, images
                      </span>
                    </p>
                  </li>
                  <li>
                    <p>
                      <CheckCircleFilled
                        style={{ color: "#31A652", paddingRight: "10px" }}
                      />
                      View exception reports
                    </p>
                  </li>
                  <li>
                    <p>
                      <CheckCircleFilled
                        style={{ color: "#31A652", paddingRight: "10px" }}
                      />
                      Manage your student’s contributions
                    </p>
                  </li>
                </ul>
              </div>
              <div className="services-btn">
                <Link to="/">Learn More</Link>
              </div>
            </div>
          </div>
        </section>
      </main>
      <div className="banner-bottom">
        <div className="banner-bottom-title">
          <div className="banner-bottom-title-header">
            <h4>
              Start growing with <br />
              the community
            </h4>
          </div>
          <div className="banner-bottom-title-btn">
            <Link to="/">Let’s Start</Link>
          </div>
        </div>
        <div className="bannerimage">
          <img src={bannerbottom} alt="bannerbottom" />
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default Home;
