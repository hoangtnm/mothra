import React, { useState } from "react";
import "antd/dist/antd.css";
import {
  Form,
  Checkbox,
  Button,
  Upload,
  message,
  Input,
  Breadcrumb,
} from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import BasicLayout from "../../components/BasicLayout";
import styles from "./ContributionUpload.module.css";
import { useSelector } from "react-redux";
import contributionAPI from "../../services/contributionAPI";
import { useParams } from "react-router-dom";
import FormData from "form-data";
import { useHistory } from "react-router-dom";

const { Dragger } = Upload;

// const itemRender = (route, params, routes, paths) => {
//   const last = routes.indexOf(route) === routes.length - 1;
//   return last ? (
//     <span>{route.breadcrumbName}</span>
//   ) : (
//     <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
//   );
// };
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

const normFile = (e) => {
  if (Array.isArray(e)) return e;
  return e && e.fileList;
};
const ContributionUpload = () => {
  const success = () => {
    message.success("Upload sucessfully!!");
  };
  const { topicId } = useParams();
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  const articleValidation = {
    beforeUpload: (file) => {
      const supportedTypes = [
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword",
      ];

      if (!supportedTypes.includes(file.type)) {
        message.error("You can only upload DOC/PDF file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 5;
      if (!isLt2M) {
        message.error("Article must smaller than 5MB!");
      }
      return !supportedTypes.includes(file.type) && isLt2M;
    },
  };

  const imageValidation = {
    beforeUpload: (file) => {
      const supportedTypes = ["image/jpeg", "image/png", "image/jpg"];
      if (!supportedTypes.includes(file.type)) {
        message.error("You can only upload JPG/PNG file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 5;
      if (!isLt2M) {
        message.error("Image must smaller than 5MB!");
      }
      return !supportedTypes.includes(file.type) && isLt2M;
    },
  };

  const [isChecked, setChecked] = useState(false);
  const checksubmit = () => setChecked(!isChecked);
  const [isUploadFail, setUploadFail] = useState(false);
  const onFinish = async (values) => {
    try {
      const form = new FormData();
      const { title, abstract, articles, images } = values;
      form.append("topicId", topicId);
      form.append("title", title);
      form.append("abstract", abstract);
      articles.forEach((item) => form.append("articles", item.originFileObj));
      if (images)
        images.forEach((item) => form.append("images", item.originFileObj));
      const contribution = await contributionAPI.createContribution(form);
      await contributionAPI.sendMailByContributionId(contribution._id);
      success();
      history.push("/contributions/management");
    } catch (err) {
      console.log(err.response.data);
      message.error(err.response.data.error);
    }
  };

  const onFinishFailed = (values, errorFields, outOfDate) => {
    console.log("Received values of form: ", values);
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        style={{ padding: 24, marginBottom: 10 }}
        className={styles.site_layout_background}
      >
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Home</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/contributions/management">Contributions</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Upload</Breadcrumb.Item>
        </Breadcrumb>
        <h2 className={styles.h2}>Upload a New Contribution</h2>
      </div>
      <div className={styles.site_layout_background} style={{ padding: 24 }}>
        <Form
          name="upload"
          {...formItemLayout}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            name="title"
            label="Title"
            rules={[
              {
                required: true,
                message: "Please input your article title",
              },
            ]}
          >
            <Input placeholder="Please input your article title" />
          </Form.Item>

          <Form.Item
            label="Articles"
            name="articles"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[
              {
                required: true,
                message: "Please upload your article",
              },
            ]}
          >
            <Dragger
              name="articleFiles"
              {...articleValidation}
              maxCount={3}
              className={styles.UploadDragger}
              // accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword"
            >
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">
                Click or drag file to this area to upload
              </p>
              <p className="ant-upload-hint">Limit 5MB per file - PDF, DOCX</p>
            </Dragger>
          </Form.Item>

          <Form.Item
            label="Images"
            name="images"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Dragger
              name="imageFiles"
              {...imageValidation}
              maxCount={3}
              className={styles.UploadDragger}
              // accept="image/png, image/jpeg, image/jpg"
            >
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">
                Click or drag file to this area to upload
              </p>
              <p className="ant-upload-hint">
                Limit 5MB per file - PNG, JPG, JPEG
              </p>
            </Dragger>
          </Form.Item>
          <Form.Item
            label="Abstract"
            name="abstract"
            rules={[
              {
                required: true,
                message: "Please input your description",
              },
              {
                min: 150,
                message: "Description must be at least 150 characters",
              },
            ]}
          >
            <Input.TextArea />
          </Form.Item>

          <Form.Item className={styles.checksubmit}>
            <div className={styles.checkbox}>
              <Checkbox onClick={checksubmit} className={styles.checkbox_btn}>
                I have read and agree to{" "}
                <Link to="/">the Terms and Conditions</Link>
              </Checkbox>
            </div>

            <div className={styles.submitlayout}>
              {isUploadFail ? (
                <p>There was a problem, please try again later!</p>
              ) : null}
              {isChecked ? (
                <Button
                  tyle="primary"
                  htmlType="submit"
                  className={styles.submitbtn}
                >
                  Submit
                </Button>
              ) : (
                <Button htmlType="primary" disabled>
                  Submit
                </Button>
              )}
            </div>
          </Form.Item>
        </Form>
      </div>
    </BasicLayout>
  );
};
export default ContributionUpload;
