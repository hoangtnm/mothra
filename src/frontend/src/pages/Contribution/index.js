import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import ContributionDetail from "./ContributionDetail";
import ContributionManagement from "./ContributionManagement";
import ContributionUpload from "./ContributionUpload";
import EditContribution from "./EditContribution";
const Contribution = ({ match }) => {
  return (
    <Switch>
      <Route exact path={`${match.path}`} component={ContributionManagement} />
      <Route
        path={`${match.path}/management`}
        component={ContributionManagement}
      />
      <Route
        path={`${match.path}/:contributionId/editContribution`}
        component={EditContribution}
      />
      <Route
        exact
        path={`${match.path}/:contributionId`}
        component={ContributionDetail}
      />

      {/* <Route path={`${match.path}/upload`} component={ContributionUpload} /> */}
    </Switch>
  );
};

export default Contribution;
