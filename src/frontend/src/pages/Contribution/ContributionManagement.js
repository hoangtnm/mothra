import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  Input,
  Space,
  List,
  Tag,
  Button,
  Card,
  Tabs,
  Breadcrumb,
  message,
} from "antd";
import { Link } from "react-router-dom";
import {
  DownloadOutlined,
  RightOutlined,
  MessageOutlined,
  ExclamationCircleFilled,
} from "@ant-design/icons";
import styles from "./ContributionManagement.module.css";
import layoutStyles from "styles/BasicLayout.module.css";
import BasicLayout from "../../components/BasicLayout";
import { useSelector } from "react-redux";
import topicAPI from "services/topicAPI";
import contributionAPI from "services/contributionAPI";
import facultyConverter from "utils/facultyConversion";
import FileDownload from "js-file-download";
import standardStyles from "styles/StandardStyles.module.css";
import Checkbox from "antd/lib/checkbox/Checkbox";

const { CheckableTag } = Tag;
const { Meta } = Card;
const { TabPane } = Tabs;
const routes = [
  {
    path: `/`,
    breadcrumbName: "Home",
  },
  {
    path: "/contributions/management",
    breadcrumbName: "Contributions",
  },
];
const itemRender = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
  );
};

const faculties = [
  { name: "Business School", alias: "bus" },
  { name: "Faculty of Education, Health and Human Science", alias: "ehhs" },
  { name: "Faculty of Engineering and Science", alias: "es" },
  { name: "Faculty of  Liberal and Sciences", alias: "las" },
];

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

const Management = () => {
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  // TODO (hoangtnm): refactor
  const [fetchedTopics, setFetchedTopics] = useState([]);
  const [selectedFaculty, setSelectedFaculty] = useState();
  const [topicsOfSelectedFaculty, setTopicsOfSelectedFaculty] = useState([]);
  const [
    contributionsOfSelectedFaculty,
    setContributionsOfSelectedFaculty,
  ] = useState([]);
  const [selectedTopics, setSelectedTopics] = useState([]);
  const [selectedContributions, setSelectedContributions] = useState([]);
  const [searchValue, setSearchValue] = useState();
  // const [page, setPage] = useState();
  const userRole = userInfo.role;
  // const dateFormat = "YYYY-MM-DD";
  // const d = new Date();

  useEffect(() => {
    let mounted = true;
    const fetchData = async () => {
      try {
        const { topics } = await topicAPI.getProcessByYear(
          new Date().getFullYear(),
          userInfo.accessToken
        );
        const tmpFaculty = topics[0].faculty;
        const tmpTopics = topics.filter(
          (topic) => topic.faculty === tmpFaculty
        );
        if (mounted) {
          setFetchedTopics(topics);
          setSelectedFaculty(tmpFaculty);
          setTopicsOfSelectedFaculty(tmpTopics);
          setContributionsOfSelectedFaculty(
            tmpTopics
              .map((topic) => topic.contributions)
              .filter((contributions) => contributions.length > 0)
              .flat()
          );
        }
      } catch (err) {
        console.log({ err });
      }
    };
    fetchData();
    return () => (mounted = false);
  }, [userInfo.accessToken]);

  const TopicAvailable = () => (
    <div className={styles.cardContainer}>
      {topicsOfSelectedFaculty.map((topic) => (
        <Card
          key={topic._id}
          className={styles.topicCard}
          actions={[
            <Button
              type="primary"
              shape="round"
              size="middle"
              style={{
                float: "right",
                marginRight: 3,
              }}
              onClick={() => history.push(`/topics/${topic._id}`)}
            >
              View <RightOutlined />
            </Button>,
          ]}
        >
          <Meta
            title={topic.title}
            description={`${topic.description.slice(0, 50)}...\n
            Closure date: ${topic.closureDate}`} //${topic.description}\n
          />
        </Card>
      ))}
    </div>
  );

  const handleSelectTopicToFilter = (topic, checked) => {
    const nextSelectedTopics = checked
      ? [...selectedTopics, topic]
      : selectedTopics.filter((t) => t._id !== topic._id);
    const tmpSelectedContributions = nextSelectedTopics
      .map((t) => t.contributions)
      .filter((contributions) => contributions.length > 0)
      .flat();
    setSelectedTopics(nextSelectedTopics);
    setSelectedContributions(tmpSelectedContributions);
  };

  const onFacultyTabClick = async (faculty) => {
    try {
      setSelectedFaculty(faculty);
      const tmpTopics = fetchedTopics.filter(
        (topic) => topic.faculty === faculty
      );
      setTopicsOfSelectedFaculty(tmpTopics);
      setContributionsOfSelectedFaculty(
        tmpTopics
          .map((topic) => topic.contributions)
          .filter((contributions) => contributions.length > 0)
          .flat()
      );
    } catch (err) {
      console.log(err);
    }
  };
  const FacultyTab = () => (
    <Tabs
      defaultActiveKey={selectedFaculty}
      onTabClick={onFacultyTabClick}
      tabBarStyle={{ marginBottom: 0 }}
    >
      {faculties.map((faculty) => (
        <TabPane key={faculty.alias} tab={faculty.name}></TabPane>
      ))}
    </Tabs>
  );
  const onDownloadAll = async () => {
    if (selectedContributions.length > 0) {
      const downloadPromises = selectedContributions.map((item) => {
        const res = contributionAPI.downloadContributionById(item._id);
        FileDownload(res, `${item.author}_${item.title}_${item._id}.zip`);
      });
      Promise.all(downloadPromises);
    } else {
      const downloadPromises = contributionsOfSelectedFaculty.map((item) => {
        const res = contributionAPI.downloadContributionById(item._id);
        FileDownload(res, `${item.author}_${item.title}_${item._id}.zip`);
      });
      Promise.all(downloadPromises);
    }
  };

  const onDownload = async (item) => {
    try {
      const res = await contributionAPI.downloadContributionById(item._id);
      FileDownload(res, `${item.author}_${item.title}_${item._id}.zip`);
    } catch (err) {
      message.error(err.response.data.error);
    }
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <div
        className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
      >
        <Breadcrumb itemRender={itemRender} routes={routes} />
        <p className={styles.title}>Contributions</p>
        <Input
          placeholder="Search by title"
          className={styles.searchInput}
          value={searchValue}
          onChange={(e) => {
            const searchInput = e.target.value;
            setSearchValue(searchInput);
            if (searchInput !== "") {
              setContributionsOfSelectedFaculty(
                fetchedTopics
                  .map((topic) => topic.contributions)
                  .filter((contributions) => contributions.length > 0)
                  .flat()
                  .filter((contribution) =>
                    contribution.title
                      .toLowerCase()
                      .includes(searchInput.toLowerCase())
                  )
              );
            } else {
              setContributionsOfSelectedFaculty(
                fetchedTopics
                  .filter((topic) => topic.faculty === selectedFaculty)
                  .map((topic) => topic.contributions)
                  .filter((contributions) => contributions.length > 0)
                  .flat()
              );
            }
          }}
        />
        {["admin", "marketing manager"].includes(userRole) && <FacultyTab />}
      </div>
      <div className={layoutStyles.layout}>
        <div className={layoutStyles.layoutContent}>
          <Card>
            <span style={{ fontWeight: "600" }}>Filter by topic:</span>
            {topicsOfSelectedFaculty.map((topic) => (
              <CheckableTag
                key={topic._id}
                checked={selectedTopics.indexOf(topic) > -1}
                onChange={(checked) =>
                  handleSelectTopicToFilter(topic, checked)
                }
                style={{ marginLeft: "1em" }}
              >
                {topic.title}
              </CheckableTag>
            ))}
          </Card>
          {["student"].includes(userRole) && (
            <div className={styles.site_layout_background}>
              <div>
                <span
                  style={{
                    fontWeight: "600",
                    fontSize: "24px",
                    padding: "50px 0px 0px 20px",
                  }}
                >
                  Available Topics
                </span>
              </div>
              {["student"].includes(userRole) && <TopicAvailable />}
            </div>
          )}
          <div
            className={styles.site_layout_background}
            style={{ padding: 24, marginTop: "1em" }}
          >
            <Space direction="vertical" style={{ width: "100%" }}>
              {selectedContributions && (
                <Button
                  type="primary"
                  shape="round"
                  icon={<DownloadOutlined />}
                  size="middle"
                  style={{
                    width: 170,
                    float: "right",
                  }}
                  onClick={onDownloadAll}
                >
                  <p
                    style={{
                      display: "inline-block",
                      marginLeft: 5,
                    }}
                  >
                    Download all (
                    {selectedTopics.length > 0
                      ? selectedContributions.length
                      : contributionsOfSelectedFaculty.length}
                    )
                  </p>
                </Button>
              )}
              <List
                itemLayout="vertical"
                size="large"
                pagination={{
                  // onChange: (page) => setPage(page),
                  pageSize: 5,
                }}
                dataSource={
                  selectedTopics.length > 0
                    ? selectedContributions
                    : contributionsOfSelectedFaculty
                }
                renderItem={(item) => (
                  <List.Item
                    key={item._id}
                    actions={[
                      // <div onChange={() => onCheck(item)}>
                      //   <IconText
                      //     icon={Checkbox}
                      //     text="check"
                      //     defaultChecked={true}
                      //     key="list-vertical-message"
                      //   />
                      // </div>,
                      <Checkbox defaultChecked={item.isSelected} disabled>
                        Selected for Publication
                      </Checkbox>,
                      <IconText
                        icon={MessageOutlined}
                        text={item.comments.length}
                        key="list-vertical-message"
                      />,
                      <div onClick={() => onDownload(item)}>
                        {" "}
                        <IconText
                          icon={DownloadOutlined}
                          text="download"
                          key="list-vertical-message"
                        />
                      </div>,
                    ]}
                  >
                    <List.Item.Meta
                      title={
                        <Link to={`/contributions/${item._id}`}>
                          {item.title}
                        </Link>
                      }
                      description={
                        <div>
                          <Tag color="blue">
                            {facultyConverter[selectedFaculty]}
                          </Tag>
                          <Tag color="default">{item.title}</Tag>
                          {item.comments.length === 0 && (
                            <Tag
                              icon={<ExclamationCircleFilled />}
                              color="warning"
                            >
                              No comment
                            </Tag>
                          )}
                        </div>
                      }
                    />
                    {item.abstract}
                    <div className={layoutStyles.cardListItemContentIndexExtra}>
                      <span
                        stype
                      >{`${item.author.firstName} ${item.author.lastName} (${item.author.username})`}</span>
                      <em className={layoutStyles.em}>{item.submitedAt}</em>
                    </div>
                  </List.Item>
                )}
              />
            </Space>
          </div>
        </div>
      </div>
    </BasicLayout>
  );
};

export default Management;
