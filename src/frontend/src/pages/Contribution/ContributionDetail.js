import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  EditOutlined,
  SendOutlined,
  FileWordTwoTone,
  FilePdfTwoTone,
  FileImageTwoTone,
  DownloadOutlined,
} from "@ant-design/icons";
import styles from "./ContributionDetail.module.css";
import BasicLayout from "../../components/BasicLayout";
import { Link } from "react-router-dom";
import {
  Avatar,
  Breadcrumb,
  Card,
  Checkbox,
  Descriptions,
  Input,
  List,
  Form,
  Button,
  Skeleton,
  message,
} from "antd";
import "antd/dist/antd.css";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import contributionAPI from "services/contributionAPI";
import standardStyles from "styles/StandardStyles.module.css";
import layoutStyles from "styles/BasicLayout.module.css";
import { parseTime } from "utils/time";
import _ from "lodash";
import IconText from "components/IconText";
import FileDownload from "js-file-download";

const { TextArea } = Input;

const ContributionDetail = () => {
  const history = useHistory();
  const { contributionId } = useParams();
  const userInfo = useSelector((state) => state.user);
  const [isFetching, setIsFetching] = useState(true);
  const [contribution, setContribution] = useState();
  const [contributionComments, setContributionComments] = useState([{}]);
  const parseFullName = (user) => `${user.firstName} ${user.lastName}`;
  const getIcon = (file, style) => {
    const filename = _.lowerCase(file);
    if (filename.endsWith("doc") || filename.endsWith("docx"))
      return FileWordTwoTone;
    else if (filename.endsWith("pdf")) return FilePdfTwoTone;
    else return FileImageTwoTone;
  };

  useEffect(() => {
    let isMounted = true;
    const fetchData = async () => {
      try {
        setIsFetching(true);
        const res = await contributionAPI.getContributionById(contributionId);
        if (isMounted) {
          setContribution(res);
          setContributionComments(res.comments);
          setIsFetching(false);
        }
      } catch (err) {
        message.error(err.response.data.error);
      }
    };
    fetchData();
    return () => (isMounted = false);
  }, [contributionId]);

  const routes = [
    {
      path: `/`,
      breadcrumbName: "Home",
    },
    {
      path: `/management`,
      breadcrumbName: "Contributions",
    },
    {
      path: "/:contributionId",
      breadcrumbName: `${isFetching ? contributionId : contribution.title}`,
    },
  ];
  const itemRender = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <Link to={paths.join("/")}>{route.breadcrumbName}</Link>
    );
  };

  const onCommentSubmited = async (values) => {
    try {
      await contributionAPI.createComment(contributionId, values.comment);
      const res = await contributionAPI.getContributionById(contributionId);
      setContributionComments(res.comments);
    } catch (err) {
      console.log({ err });
    }
  };

  const onCheck = async (e, id) => {
    const isChecked = e.target.checked;
    const res = await contributionAPI.toggleIsSelected(id, isChecked);
    setContribution(res);
  };
  const onDownload = async (id) => {
    try {
      const res = await contributionAPI.downloadContributionById(id);
      FileDownload(res, `${contributionId}.zip`);
    } catch (err) {
      message.error(err.response.data.error);
    }
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      {!isFetching ? (
        <>
          <div
            className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
          >
            <Breadcrumb
              itemRender={itemRender}
              routes={routes}
              className={standardStyles.breadcrumb}
            />
            <div className={standardStyles.pageHeaderHeading}>
              <div className={standardStyles.pageHeaderHeadingTitle}>
                Contribution Details
              </div>
            </div>
            <div className={standardStyles.pageHeaderContent}>
              <Descriptions size="small" column={1}>
                <Descriptions.Item label="Author">
                  {parseFullName(contribution.author)}
                </Descriptions.Item>
                <Descriptions.Item label="Creation Time">
                  {parseTime(contribution.createdAt)}
                </Descriptions.Item>
              </Descriptions>
            </div>
          </div>
          <div className={layoutStyles.layout}>
            <div className={layoutStyles.layoutContent}>
              <Card
                className={styles.site_layout_background}
                title="Your Work"
                extra={
                  userInfo.role === "student" && (
                    <Button
                      htmlType="submit"
                      type="primary"
                      style={{
                        borderRadius: "8px",
                        float: "right",
                      }}
                      onClick={() =>
                        history.push(
                          `/contributions/${contributionId}/editContribution`
                        )
                      }
                      icon={<EditOutlined style={{ fontSize: "14px" }} />}
                    >
                      Update
                    </Button>
                  )
                }
                actions={[
                  ["admin", "marketing coordinator"].includes(
                    userInfo.role
                  ) && (
                    <Checkbox
                      defaultChecked={contribution.isSelected}
                      onChange={(e) => onCheck(e, contribution._id)}
                    >
                      Select for Publication
                    </Checkbox>
                  ),
                  <div onClick={() => onDownload(contributionId)}>
                    <IconText icon={DownloadOutlined} text="Download" />
                  </div>,
                ]}
              >
                <div className={styles.title}>Abstract</div>
                <p>{contribution.abstract}</p>
                <Link to="#">
                  <List
                    size="medium"
                    dataSource={contribution.articles}
                    style={{
                      wordBreak: "break-all",
                    }}
                    renderItem={(item) => (
                      <List.Item>
                        <IconText
                          key={item}
                          icon={getIcon(item)}
                          text={item}
                          style={{ fontSize: "24px" }}
                          twoToneColor="#08c"
                        />
                      </List.Item>
                    )}
                  />{" "}
                </Link>
                <Link to="#">
                  <List
                    size="medium"
                    dataSource={contribution.images}
                    renderItem={(item) => (
                      <List.Item>
                        <IconText
                          key={item}
                          icon={getIcon(item)}
                          text={item}
                          style={{ fontSize: "24px" }}
                          twoToneColor="#ffc93c"
                        />
                      </List.Item>
                    )}
                  />{" "}
                </Link>
              </Card>
              {(userInfo.role === "student" ||
                userInfo.role === "marketing coordinator") && (
                <div className={styles.site_layout_comment}>
                  <div className={styles.title}>Private Comments</div>
                  {contributionComments[0].postedBy && (
                    <List
                      style={{ maxHeight: 200, overflow: "auto" }}
                      dataSource={contributionComments}
                      renderItem={(item) => (
                        <List.Item>
                          <List.Item.Meta
                            avatar={
                              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                            }
                            title={parseFullName(item.postedBy)}
                            description={item.comment}
                          />
                        </List.Item>
                      )}
                    />
                  )}
                  <Form onFinish={onCommentSubmited}>
                    <Form.Item name="comment" required>
                      <TextArea
                        showCount
                        maxLength={250}
                        rows={2}
                        allowClear
                        placeholder="Add Private Comments..."
                        style={{ borderRadius: "10px", marginTop: "10px" }}
                        required
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        htmlType="submit"
                        type="primary"
                        style={{ borderRadius: "8px" }}
                      >
                        Send <SendOutlined style={{ fontSize: "14px" }} />
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              )}
            </div>
          </div>
        </>
      ) : (
        <Skeleton />
      )}
    </BasicLayout>
  );
};
export default ContributionDetail;
