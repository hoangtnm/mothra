import React, { useState, useEffect } from "react";
import "antd/dist/antd.css";
import {
  Form,
  Checkbox,
  Button,
  Upload,
  message,
  Input,
  Breadcrumb,
  Modal,
  Descriptions,
  Skeleton,
  Card,
} from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import BasicLayout from "../../components/BasicLayout";
import styles from "./EditContribution.module.css";
import { useSelector } from "react-redux";
import contributionAPI from "../../services/contributionAPI";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import standardStyles from "styles/StandardStyles.module.css";
import layoutStyles from "styles/BasicLayout.module.css";

const { Dragger } = Upload;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

const normFile = (e) => {
  if (Array.isArray(e)) return e;
  return e && e.fileList;
};
const data = {
  title: "",
  articles: null,
  images: null,
  abstract: "",
};
const EditContribution = () => {
  const { contributionId } = useParams();
  const history = useHistory();
  const userInfo = useSelector((state) => state.user);
  const imageValidation = {
    beforeUpload: (file) => {
      const supportedTypes = ["image/jpeg", "image/png", "image/jpg"];
      if (!supportedTypes.includes(file.type)) {
        message.error("You can only upload JPG/PNG file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 5;
      if (!isLt2M) {
        message.error("Image must smaller than 5MB!");
      }
      return !supportedTypes.includes(file.type) && isLt2M;
    },
  };
  const articleValidation = {
    beforeUpload: (file) => {
      const supportedTypes = [
        "application/pdf",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/msword",
      ];

      if (!supportedTypes.includes(file.type)) {
        message.error("You can only upload DOC/PDF file!");
      }
      const isLt2M = file.size / 1024 / 1024 < 5;
      if (!isLt2M) {
        message.error("Article must smaller than 5MB!");
      }
      return !supportedTypes.includes(file.type) && isLt2M;
    },
  };
  const success = () => {
    Modal.success({
      content: "Edit contribution sucessfully!!",
    });
  };
  const [isChecked, setChecked] = useState(false);
  const checksubmit = () => setChecked(!isChecked);
  const [isUploadFail, setUploadFail] = useState(false);
  const [isEditable, setEditable] = useState(true);
  const [form] = Form.useForm();
  const [contribution, setContribution] = useState();
  const [imageFileList, setImageFileList] = useState([]);
  const [articleFileList, setArticleFileList] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  useEffect(() => {
    let isMounted = true;
    const fetchData = async () => {
      try {
        userInfo.role === "student" ? setEditable(false) : setEditable(true);
        const contribution = await contributionAPI.getContributionById(
          contributionId
        );
        data.title = contribution.title;
        data.articles = contribution.articles;
        data.abstract = contribution.abstract;
        const tmp = [];
        contribution.images.forEach((image) => {
          tmp.push({
            uid: contribution.images.indexOf(image),
            name: image,
            status: "done",
          });
        });
        const tmp2 = [];
        contribution.articles.forEach((article) => {
          tmp2.push({
            uid: contribution.articles.indexOf(article),
            name: article,
            status: "done",
          });
        });
        if (isMounted) {
          setContribution(contribution);
          setArticleFileList(tmp2);
          setImageFileList(tmp);
          setIsFetching(false);
        }
      } catch (err) {
        message.error(err.response.data.error);
        history.push("/404");
      }
    };
    fetchData();
    return () => (isMounted = false);
  }, [contributionId]);
  const onFinish = async (values) => {
    try {
      const form = new FormData();
      const { title, abstract, articles } = values;
      form.append("contributionId", contributionId);
      form.append("title", title);
      form.append("abstract", abstract);
      if (articles)
        articles.forEach((item) => form.append("articles", item.originFileObj));
      const contribution = await contributionAPI.updateContributionById(
        contributionId,
        form
      );
      await contributionAPI.sendMailByContributionId(contribution._id);
      success();
      history.push("/contributions/management");
    } catch (err) {
      console.log(err.response.data);
      message.error(err.response.data.error);
    }
  };

  const onFinishFailed = (values) => {
    console.log("Received values of form: ", values);
    console.log(imageFileList);
  };

  return (
    <BasicLayout role={userInfo.role} username={userInfo.username}>
      <>
        <div
          className={`${standardStyles.pageHeader} ${standardStyles.hasBreadcrumb}`}
        >
          <Breadcrumb>
            <Breadcrumb.Item>
              <Link to="/">Home</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <Link to="/contributions/management">Contributions</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <Link to={contributionId}>
                {isFetching ? contributionId : contribution.title}
              </Link>
            </Breadcrumb.Item>
          </Breadcrumb>
          <div className={standardStyles.pageHeaderHeading}>
            <div className={standardStyles.pageHeaderHeadingTitle}>
              Update Contribution
            </div>
          </div>
          <div className={standardStyles.pageHeaderContent}>
            <Descriptions size="small" column={1}>
              <Descriptions.Item label="Note">
                <p>
                  Updates can continues to be done until the final closure date.
                </p>
              </Descriptions.Item>
            </Descriptions>
          </div>
        </div>
      </>
      {!isFetching ? (
        <div className={layoutStyles.layout}>
          <div className={layoutStyles.layoutContent}>
            <Card>
              <Form
                name="upload"
                {...formItemLayout}
                form={form}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                initialValues={{
                  title: contribution.title,
                  abstract: contribution.abstract,
                }}
              >
                <Form.Item
                  name="title"
                  label="Title"
                  disabled={isEditable}
                  rules={[
                    {
                      required: true,
                      message: "Please input your article title",
                    },
                  ]}
                >
                  <Input placeholder="Please input your article title" />
                </Form.Item>

                <Form.Item
                  label="Articles"
                  name="articles"
                  valuePropName="articleFileList"
                  disabled={isEditable}
                  getValueFromEvent={normFile}
                  rules={[
                    {
                      required: false,
                      message: "Please upload your article",
                    },
                  ]}
                >
                  <Dragger
                    name="articleFiles"
                    {...articleValidation}
                    defaultFileList={[...articleFileList]}
                    maxCount={3}
                    className={styles.UploadDragger}
                    accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword"
                  >
                    <p className="ant-upload-drag-icon">
                      <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint">
                      Limit 20MB per file - PDF, DOCX
                    </p>
                  </Dragger>
                </Form.Item>

                <Form.Item
                  label="Images"
                  name="images"
                  disabled={isEditable}
                  valuePropName="imageFileList"
                  getValueFromEvent={normFile}
                >
                  <Dragger
                    name="imageFiles"
                    {...imageValidation}
                    defaultFileList={[...imageFileList]}
                    maxCount={3}
                    className={styles.UploadDragger}
                    accept="image/png, image/jpeg, image/jpg"
                  >
                    <p className="ant-upload-drag-icon">
                      <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">
                      Click or drag file to this area to upload
                    </p>
                    <p className="ant-upload-hint">
                      Limit 5MB per file - PNG, JPG, JPEG
                    </p>
                  </Dragger>
                </Form.Item>
                <Form.Item
                  label="Abstract"
                  name="abstract"
                  disabled={isEditable}
                  rules={[
                    {
                      required: true,
                      message: "Please input your description",
                    },
                    {
                      min: 150,
                      message: "Description must be at least 150 characters",
                    },
                  ]}
                >
                  <Input.TextArea />
                </Form.Item>

                <Form.Item className={styles.checksubmit}>
                  <div className={styles.checkbox}>
                    <Checkbox
                      onClick={checksubmit}
                      className={styles.checkbox_btn}
                    >
                      I have read and agree to{" "}
                      <Link to="/">the Terms and Conditions</Link>
                    </Checkbox>
                  </div>

                  <div className={styles.submitlayout}>
                    {isUploadFail ? (
                      <p>There was a problem, please try again later!</p>
                    ) : null}
                    {isChecked ? (
                      <Button
                        tyle="primary"
                        htmlType="submit"
                        className={styles.submitbtn}
                        disabled={isEditable}
                      >
                        Update
                      </Button>
                    ) : (
                      <Button htmlType="primary" disabled>
                        Update
                      </Button>
                    )}
                  </div>
                </Form.Item>
              </Form>
            </Card>
          </div>
        </div>
      ) : (
        <Skeleton />
      )}
    </BasicLayout>
  );
};
export default EditContribution;
