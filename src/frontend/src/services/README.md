# Services <!-- omit in toc -->

## Contents <!-- omit in toc -->

## API Service

| Action | HTTP Method | Example |
| ------ | ----------- | ------- |
| Create | POST        |         |
| Read   | GET         |         |
| Update | PUT         |         |
| Delete | DELETE      |         |

## References

[1] J. Kasun, “Making Asynchronous HTTP Requests in JavaScript with Axios.” https://stackabuse.com/making-asynchronous-http-requests-in-javascript-with-axios/ (accessed Jan. 26, 2021).
