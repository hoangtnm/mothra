import ApiService from "./Api";

const BASE_URL = process.env.URL || "http://localhost:3001/api/v1";
const client = new ApiService({ baseURL: BASE_URL });
const topicAPI = {};

topicAPI.getProcessByYear = (year) => {
  return client.get(`/topics/process/${year}`);
};

topicAPI.getReports = (year) => {
  return client.get(`/topics/reports/${year}`);
};

topicAPI.createTopic = (
  title,
  description,
  faculty,
  closureDate,
  finalClosureDate
) => {
  return client.post(`/topics/upload`, {
    title,
    description,
    faculty,
    closureDate,
    finalClosureDate,
  });
};

topicAPI.getTopicById = (id) => {
  return client.get(`/topics/${id}`);
};

topicAPI.getTopicsByFaculty = (faculty) => {
  return client.get(`/topics/byFaculty/${faculty}`);
};

topicAPI.updateTopicById = (id, description, closureDate, finalClosureDate) => {
  return client.put(`/topics/${id}`, {
    description,
    closureDate,
    finalClosureDate,
  });
};

topicAPI.deleteTopic = (id) => {
  return client.delete(`/topics/${id}`);
};

export default topicAPI;
