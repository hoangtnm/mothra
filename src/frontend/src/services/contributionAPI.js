import ApiService from "./Api";

const BASE_URL = process.env.URL || "http://localhost:3001/api/v1";
const client = new ApiService({ baseURL: BASE_URL });

const contributionAPI = {};

contributionAPI.createContribution = (form) => {
  return client.post(`/contributions/upload`, form, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

contributionAPI.getContributionById = (id) => {
  return client.get(`/contributions/${id}`);
};

contributionAPI.downloadContributionById = (id) => {
  return client.get(`/contributions/${id}/download`, {
    responseType: "blob",
  });
};

contributionAPI.downloadPublicContributionById = (year, id) => {
  return client.get(`/contributions/publication/${year}/${id}/download`);
};

contributionAPI.updateContributionById = (id, form) => {
  return client.post(`/contributions/${id}/update`, form, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

contributionAPI.deleteContributionById = (id) => {
  return client.delete(`/contributions/${id}`);
};

contributionAPI.createComment = (id, comment) => {
  return client.post(`/contributions/${id}/comments`, { comment });
};
contributionAPI.getPublicationByYear = (year) => {
  return client.get(`/contributions/publication/${year}`);
};
// contributionAPI.getCommentById = (contributionId, commentId) => {
//   return client.get(`/contributions/${contributionId}/comments/${commentId}`);
// };

contributionAPI.deleteCommentById = (id, commentId) => {
  return client.delete(`/contributions/${id}/comments/${commentId}`);
};

contributionAPI.sendMailByContributionId = (contributionId) => {
  return client.post(`/mail`, { contributionId });
};

contributionAPI.toggleIsSelected = (id, isSelected) => {
  return client.post(`/contributions/${id}/toggleIsSelected`, {
    isSelected,
  });
};

export default contributionAPI;
