import ApiService from "./Api";

const BASE_URL = process.env.URL || "http://localhost:3001/api/v1";
const client = new ApiService({ baseURL: BASE_URL });

const userAPI = {};

userAPI.login = (username, password) => {
  return client.post(`/users/login`, {
    username,
    password,
  });
};

userAPI.addUser = (
  username,
  password,
  firstName,
  lastName,
  dateOfBirth,
  phone,
  email,
  faculty,
  role
) => {
  return client.post(`/users/signup`, {
    username,
    password,
    firstName,
    lastName,
    dateOfBirth,
    phone,
    email,
    faculty,
    role,
  });
};

userAPI.getUser = (username) => {
  return client.get(`/users/${username}`);
};
userAPI.getAll = () => {
  return client.get(`/users/all`);
};

userAPI.updateUser = (
  username,
  firstName,
  lastName,
  dateOfBirth,
  phone,
  email
) => {
  return client.put(`/users/${username}`, {
    username,
    firstName,
    lastName,
    dateOfBirth,
    phone,
    email,
  });
};

userAPI.updateUserPassword = (
  username,
  password,
  firstName,
  lastName,
  dateOfBirth,
  phone,
  email
) => {
  return client.put(`/users/${username}`, {
    username,
    password,
    firstName,
    lastName,
    dateOfBirth,
    phone,
    email,
  });
};

userAPI.deleteUser = (username) => {
  return client.delete(`/users/${username}`);
};

userAPI.logout = (refreshToken) => {
  return client.post(`/users/logout`, {
    refreshToken
  });
};

userAPI.logoutAll = (username) => {
  return client.post(`/users/logoutAll`, {
    username,
  });
};
// const PAGE_LIMIT = 20;
// const getPageSlice = (limit, page = 0) => ({ begin: page * limit, end: (page + 1) * limit });
// const getPageValues = ({ begin, end, items }) => items.slice(begin, end);

// baseAPI.getTopStoryIds = () => client.get(`/topstories${JSON_QUERY}`);
// baseAPI.getStory = id => client.get(`/item/${id}${JSON_QUERY}`);
// baseAPI.getStoriesByPage = (ids, page) => {
//   const { begin, end } = getPageSlice(PAGE_LIMIT, page);
//   const activeIds = getPageValues({ begin, end, items: ids });
//   const storyPromises = activeIds.map(id => MothraApi.getStory(id));
//   return Promise.all(storyPromises);
// };

export default userAPI;
