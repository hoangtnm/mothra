import axios from "axios";

// Default API will be your root
const API_ROOT = process.env.URL || "http://localhost:3000/";
const TIMEOUT = 20000;
const HEADERS = {
  "Content-Type": "application/json",
  Accept: "application/json",
  crossdomain: true,
};

class ApiService {
  constructor({
    baseURL = API_ROOT,
    timeout = TIMEOUT,
    headers = HEADERS,
    auth,
  }) {
    const client = axios.create({
      baseURL,
      timeout,
      headers,
      auth,
    });

    client.interceptors.request.use(
      (config) => {
        const accessToken = localStorage.getItem("accessToken");
        if (accessToken) {
          config.headers["authorization"] = `Bearer ${accessToken}`;
        }
        return config;
      },
      (error) => Promise.reject(error)
    );

    // client.interceptors.response.use(this.handleSuccess, this.handleError);
    client.interceptors.response.use(
      (response) => response,
      (error) => {
        const originalRequest = error.config;
        const status = error.response.status;
        if (status === 401 && originalRequest.url === "/token/refresh") {
          return Promise.reject(error);
        }
        if (status === 401 && !originalRequest._retry) {
          originalRequest._retry = true;
          console.log(`Invalid accessToken, refreshing new one...`);
          const refreshToken = localStorage.getItem("refreshToken");
          return client
            .post("/token/refresh", { refreshToken })
            .then((response) => {
              if (response.status === 201) {
                const { accessToken } = response.data;
                localStorage.setItem("accessToken", accessToken);
                client.defaults.headers[
                  "authorization"
                ] = `Bearer ${accessToken}`;
                return client(originalRequest);
              }
            })
            .catch((err) => Promise.reject(err));
        }
        return Promise.reject(error);
      }
    );
    this.client = client;
  }

  handleSuccess(response) {
    return response;
  }

  handleError(error) {
    return Promise.reject(error);
  }

  get(path, headers) {
    return this.client.get(path, headers).then((response) => response.data);
  }

  post(path, payload, headers) {
    return this.client
      .post(path, payload, headers)
      .then((response) => response.data);
  }

  put(path, payload, headers) {
    return this.client
      .put(path, payload, headers)
      .then((response) => response.data);
  }

  patch(path, payload) {
    return this.client.patch(path, payload).then((response) => response.data);
  }

  delete(path, headers) {
    return this.client.delete(path, headers).then((response) => response.data);
  }
}

export default ApiService;
