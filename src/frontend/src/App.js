import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector } from "react-redux";
import jwt from "jsonwebtoken";
import Home from "./pages/Home";
import User from "./pages/User";
import Login from "./pages/User/Login";
import Topic from "./pages/Topic";
import NoFoundPage from "./pages/404";
import NoAuthorizedPage from "./pages/403";
import Contribution from "./pages/Contribution";
import Dashboard from "./pages/Dashboard";
import Publication from "./pages/Publication";
import { useDispatch } from "react-redux";
import { setAuthenticated, saveInfo } from "./redux/userSlice";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { isAuthenticated } = useSelector((state) => state.user);
  return (
    <Route
      {...rest}
      render={(rest) =>
        isAuthenticated ? (
          <Component {...rest} />
        ) : (
          <Redirect to="/users/login" />
        )
      }
    />
  );
};

function App() {
  const dispatch = useDispatch();
  const { isAuthenticated } = useSelector((state) => state.user);
  const accessToken = localStorage.getItem("accessToken");
  const refreshToken = localStorage.getItem("refreshToken");
  if (!isAuthenticated && accessToken && refreshToken) {
    const decoded = jwt.decode(accessToken);
    dispatch(saveInfo(decoded));
    dispatch(setAuthenticated());
  }
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/home" component={Home} />
          <Route path="/users/login" component={Login} />
          <Route path="/publication/:year" component={Publication} />
          <PrivateRoute path="/users" component={User} />
          <PrivateRoute path="/contributions" component={Contribution} />
          <PrivateRoute path="/topics" component={Topic} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <Route path="/403" component={NoAuthorizedPage} />
          <Route path="/404" component={NoFoundPage} />
          <Redirect to="/404" />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
