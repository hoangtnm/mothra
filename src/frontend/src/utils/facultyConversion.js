const facultyConverter = {
  bus: "Business School",
  ehhs: "Faculty of Education, Health and Human Science",
  es: "Faculty of Engineering and Science",
  las: "Faculty of Liberal and Sciences",
};

export default facultyConverter;
