const getCurrentTime = () => {
  const d = new Date();
  const year = d.getFullYear();
  const month = d.getMonth() + 1;
  const date = d.getDate();
  const hours = d.getHours();
  const minutes = d.getMinutes();
  const seconds = d.getSeconds();
  return `${month}/${date}/${year} - ${hours}:${minutes}:${seconds}`;
};

const parseTime = (time) => {
  const d = new Date(time);
  const year = d.getFullYear();
  const month = d.getMonth() + 1;
  const date = d.getDate();
  const hours = d.getHours();
  const minutes = d.getMinutes();
  const seconds = d.getSeconds();
  return `${month}/${date}/${year} - ${hours}:${minutes}:${seconds}`;
};

export { getCurrentTime, parseTime };
